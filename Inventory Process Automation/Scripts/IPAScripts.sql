﻿--drop table PRINT_LAYOUTS;
create table PRINT_LAYOUTS(
	LayoutId int IDENTITY primary key,
	Name varchar(100) not null,
	PaperSizeH int not null,
	PaperSizeW int not null,
	BarcodeSizeH int not null,
	BarcodeSizeW int not null,
	RowsPerPage int not null,
	ColumnsPerPage int not null,
	BarcodeDirection  int not null,
	PageDirection  int not null,
	PaperMarginX int not null,
	PaperMarginY  int not null,
	BarcodePaddingX  int not null,
	BarcodePaddingY  int not null
);


--drop table BARCODE_LOG;
Create table BARCODE_LOG
(
	BarcodeID int IDENTITY PRIMARY KEY ,
	PrintInstanceId varchar(100) not null,
	SerialNumber int not null,
	SubItemCode int null,
	Barcode varchar(20) not null UNIQUE,
	CreationTime datetime not null default CURRENT_TIMESTAMP
);



--drop table PRINT_INSTANCE
Create table PRINT_INSTANCE
(
	 InstanceId int IDENTITY PRIMARY KEY ,
     RefDocIdentifier  varchar(200) not null,
     PrintlayoutId int not null,
	 ItemCode varchar(20) not null,
	 DateParam varchar(20) not null,
	 HasSubItem bit not null,
	 SubItemCount int null,
     PrintQuantity int not null,
	 CreationTime datetime not null default CURRENT_TIMESTAMP
)

--drop table STORAGE_LOCATION;
Create table STORAGE_LOCATION
(
	LocationId int IDENTITY PRIMARY KEY ,
	Aisle int not null,
	Block varchar(10) not null,
	Side varchar(10) not null,
	Rack int not null,
	Layer int not null,
	Partition varchar(10) not null,
	Barcode varchar(50) not null UNIQUE,

	CreationTime datetime not null default CURRENT_TIMESTAMP
);


--drop table PRINT_INSTANCE
CREATE table USERS
(
	UserID int IDENTITY PRIMARY KEY ,
	UserName varchar(100) not null UNIQUE,
	--UserPassword varchar(100) not null,
	FirstName varchar (50) not null,
	LastName varchar (50) not null,
	Email varchar (50) not null,
	ContactNumber int null,
	CreationTime datetime not null,
	CompanyCode varchar(50)
);


--drop table INVENTORY_ACTIVITY
create table INVENTORY_ACTIVITY
(
	ActivityId int identity primary key,
	RefDocIdentifier varchar(200) not null,
	ActivityType int not null,
	MovementQty int,
	Direction int not null,
	CreationTime datetime not null  default CURRENT_TIMESTAMP,
)

--drop table ITEM_MOVEMENT;
Create table ITEM_MOVEMENT
(
	MovementId int IDENTITY PRIMARY KEY ,
	ActivityId int not null References INVENTORY_ACTIVITY(ActivityId),
	LocationId int not null References STORAGE_LOCATION(LocationId),
	ItemBarcodeId int not null References BARCODE_LOG(BarcodeId),
	IsActive bit not null default 1,
	CreationTime datetime not null  default CURRENT_TIMESTAMP,
);

ALTER TABLE PRINT_INSTANCE ADD ItemDesc varchar(500) null;

ALTER TABLE USERS ADD  BranchCode varchar(50) not null;

alter table print_layouts add DefaultPrinter varchar(500);

alter table print_layouts add PrintResolution numeric not null default 203;

ALTER TABLE USERS ADD UserRole int not null default(1);

CREATE TABLE ERP_LOCATION (
	ID int IDENTITY primary key,
	Identifier varchar(500) not null UNIQUE
);

alter table erp_location add Name varchar(100);
alter table erp_location add Code varchar(30) unique;

alter table storage_location ADD CONSTRAINT FK_erplocation
FOREIGN KEY (Aisle) REFERENCES ERP_LOCATION(ID);


alter table USERS add BranchName varchar(50);
alter table USERS add CompanyName varchar(50);

alter table USERS add PasswordHash varchar(100) not null;
alter table USERS add PasswordSalt varchar(100) not null;


alter table BARCODE_LOG alter column Barcode varchar(50) not null;
alter table print_layouts add Barwidth numeric not null default 1;


alter table print_layouts add IncludeNameLabel bit not null default 1;
alter table print_layouts add IncludeBarcodeLabel bit not null default 1;

