﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

using IPA.Lib.Core;
using IPA.Lib.Models;
using IPA.Lib.Core.Database;
using IPA.Lib.ERP.StandardObjects;
using System.Data;
using System.Dynamic;
using Microsoft.AspNetCore.Authorization;
using IPA.Lib.Core.Authentication;

namespace IPA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseReturnController : ControllerBase
    {
        // GET: api/Data
        [Authorize]
        [HttpGet]
        public string Get()
        {
            //BarcodeCreation.PrintSeries(new string[] { "HELLOWORLD_NP_12345"}, PrintFormats.LAYOUT_THERMAL);
            return JsonConvert.SerializeObject(Printing.GetPrinters());
        }

        //gets the list of DispatchOrder documents in the system
        [HttpOptions("Documents")]
        public string documents()
        {
            return "";
        }
        [Authorize]
        [HttpGet("Documents")]
        public string Documents()
        {
            dynamic Filters = new ExpandoObject();
            Filters.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
            Filters.BranchCode = AuthManager.GetCurrentUser().BranchCode;

            List<Document> receiptItems = PurchaseReturn.GetDocuments(Filters);
            return JsonConvert.SerializeObject(receiptItems);
        }

        [HttpOptions("DocumentItems")]
        public string documentItems()
        {
            return "";
        }
        [Authorize]
        [HttpGet("DocumentItems")]
        public string DocumentItems(string VoucherNo, string FormCode)
        {
            dynamic Filters = new ExpandoObject();
            Filters.DocumentType = DocumentTypes.PURCHASE_RETURN;
            Filters.VoucherNo = VoucherNo;
            Filters.FormCode = FormCode;
            Filters.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
            Filters.BranchCode = AuthManager.GetCurrentUser().BranchCode;

            List<DocumentItem> receiptItems = PurchaseReturn.GetDocumentItems(Filters);
            return JsonConvert.SerializeObject(receiptItems);

        }
    }
}