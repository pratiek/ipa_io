﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using IPA.Lib.Core.Authentication;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Web;
using IPA.Lib.Metrics;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private IConfiguration _config;
        private object claims;

        public UsersController(IConfiguration config)
        {
            _config = config;
        }
        [HttpOptions("Signup")]
        public string Signupopt()
        {
            return "";
        }

        [Authorize]
        [HttpPost("Signup")]
        public string Signup(dynamic userData)
        {
            var expConverter = new ExpandoObjectConverter();
            var SignUpData = JsonConvert.DeserializeObject<ExpandoObject>(userData.ToString(), expConverter);

            dynamic response = new ExpandoObject();
            //response.UserData 

            string userInsert = "INSERT INTO USERS (" +
             "UserName," +
             "PasswordHash," +
             "PasswordSalt," +
             "FirstName," +
             "LastName," +
             "Email," +
             "ContactNumber," +
             "CompanyCode," +
             "BranchName," +
             "CompanyName," +
             "UserRole," +
             "BranchCode" +
         ")" +
         " Values (" +
             "@UserName," +
             "@PasswordHash," +
             "@PasswordSalt," +
             "@FirstName," +
             "@LastName," +
             "@Email," +
             "@ContactNumber," +
             "@CompanyCode," +
             "@BranchName," +
             "@CompanyName," +
             "@UserRole," +
             "@BranchCode" +
         "); ";

            PasswordHashing pwHasher = new PasswordHashing();
            HashWithSaltResult hashResultSha256 = pwHasher.HashWithSalt(SignUpData.password, 64, SHA256.Create());

            string pwsalt = hashResultSha256.Salt;
            string pwhash = hashResultSha256.Digest;


            string hashSimpe = pwHasher.GenerateHashForAuth(SignUpData.password, pwsalt, SHA256.Create());

            User user = new User {
                UserName = SignUpData.userName,
                PasswordHash = pwhash,
                PasswordSalt = pwsalt,
                FirstName = SignUpData.firstName,
                LastName = SignUpData.lastName,
                Email = SignUpData.email,
                UserRole = (USER_ROLES)SignUpData.userRole,
                ContactNumber = SignUpData.contactNumber,
                CompanyCode = SignUpData.companyCode,
                BranchCode = SignUpData.branchCode,
                BranchName = SignUpData.branchName,
                CompanyName = SignUpData.companyName
            };
            //foreach (ItemBarcode ibc in BarcodesToPrint) {
            try {
                DBManager.InsertObject<User>(userInsert, user);
                response.success = true;
                response.message = "Registered successfully!";
                } 
            catch (Exception ex)
            {
                response.success = false;
                response.message = ex.Message;
            }
            return JsonConvert.SerializeObject(response);
        }

       
        [HttpOptions("Login")]
        public string loginopt()
        {
            return "";
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public string Login(dynamic userData)
        {
            var currentUser = HttpContext.User;
            var expConverter = new ExpandoObjectConverter();
            var SignUpData = JsonConvert.DeserializeObject<ExpandoObject>(userData.ToString(), expConverter);

            dynamic response = new ExpandoObject();

            if(Enumerable.Count(DBManager.SelectObjects<User>("select * from users WHERE UserName='" + SignUpData.username + "'")) > 0)
            {
                User usr = Enumerable.First(DBManager.SelectObjects<User>("select * from users WHERE UserName='" + SignUpData.username + "'"));//? "YESS" : "NOO";

                PasswordHashing pwHasher = new PasswordHashing();
                string hashResultSha256 = pwHasher.GenerateHashForAuth(SignUpData.password, usr.PasswordSalt, SHA256.Create());





                //string test1 = pwHasher.GenerateHashForAuth("pradeeps", "/7iHCUnzKjRp6aRKNgyKJualRoR6Pfk8wzjCfGxCc/+YvQkR5I2CZMS3OQEtBIdvpDTKHoWFVY8z7pX+SDwgCrQ====", SHA256.Create());
                //HashWithSaltResult testHash = pwHasher.HashWithSalt("pradeeps", 64, SHA256.Create());
                // string hash = testHash.Digest;
                //string salt = testHash.Salt;




                if (hashResultSha256 == usr.PasswordHash)
                {
                    response.success = true;
                    var tokenString = GenerateJSONWebToken(usr);
                    response = Ok(new { token = tokenString });
                    response = response.Value;
                    //DateTime date = DateTime.Parse(currentUser.Claims.FirstOrDefault(c => c.Type == "DateOfJoing").Value);
                    //var username = currentUser.Claims.FirstOrDefault(c => c.Type == "Username").Value;

                    //Console.Write(response);
                    
                }
                else
                {
                    response.success = false;
                    response.message = "Incorrect Username or Password!";
                }
            } else
            {
                response.success = false;
                response.message = "Username not registered!";
            }
            

            return JsonConvert.SerializeObject(response);

            //response.UserData 
            //return JsonConvert.SerializeObject(response);
        }

        private string GenerateJSONWebToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            Console.Write(userInfo);
            var claims = new[] {
                new Claim("UserID", userInfo.UserId.ToString())
            };

            ClaimsIdentity identity = new ClaimsIdentity(claims, "Details");
            // AuthenticationTypes.Basic

            var principal = new ClaimsPrincipal(identity);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [HttpOptions("GetUser")]
        public string getUser()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetUser")]
        public string GetUser()
        {
            var user = AuthManager.GetCurrentUser();
            return JsonConvert.SerializeObject(user);
        }

  
        [HttpOptions("UsersList")]
        public string usersListOpt()
        {
            return "";
        }

        [Authorize]
        [HttpGet("UsersList")]
        public string UsersList()
        {
            dynamic response = new ExpandoObject();
            List<User> users = DBManager.SelectObjects<User>("select * from USERS;");
            response = users;
            return JsonConvert.SerializeObject(response);
        }

        [HttpOptions("DeleteUser")]
        public string DeleteUser()
        {
            return "";
        }

        [Authorize]
        [HttpPost("DeleteUser")]
        public string DeleteUser(dynamic userData)
        {
            var expConverter = new ExpandoObjectConverter();
            var SignUpData = JsonConvert.DeserializeObject<ExpandoObject>(userData.ToString(), expConverter);

            dynamic response = new ExpandoObject();

            if (Enumerable.Count(DBManager.SelectObjects<User>("select * from users WHERE UserName='" + SignUpData.UserName + "'")) > 0)
            {
                //User usr = Enumerable.First(DBManager.SelectObjects<User>("delete from users WHERE UserID='" + SignUpData.UserId + "'"));//? "YESS" : "NOO"
                DBManager.SelectObjects<User>("delete from users WHERE UserID='" + SignUpData.UserId + "'");
            }
            else
            {
                response.success = false;
                response.message = "Operation not successfull";
            }


            return JsonConvert.SerializeObject(response);

            //response.UserData 
            //return JsonConvert.SerializeObject(response);
        }

        [HttpOptions("UpdateUser")]
        public string updateUser()
        {
            return "";
        }

        [Authorize]
        [HttpPost("UpdateUser")]
        public string UpdateUser(User userData)
        {
            var userID = Convert.ToInt64(userData.UserId);
            var update = IPA.Lib.Models.User.UpdateUserById(userID, userData);
            return JsonConvert.SerializeObject(update);
        }

        [HttpOptions("UpdatePassword")]
        public string updatePassword()
        {
            return "";
        }

        [Authorize]
        [HttpPost("UpdatePassword")]
        public string UpdatePassword(dynamic userData)
        {
            var newPassword = (userData.Password).ToString();
            var userId = (long)(userData.UserId);
            PasswordHashing pwHasher = new PasswordHashing();
            HashWithSaltResult hashResultSha256 = pwHasher.HashWithSalt(newPassword, 64, SHA256.Create());

            string pwsalt = hashResultSha256.Salt;
            string pwhash = hashResultSha256.Digest;

            var update = IPA.Lib.Models.User.UpdateUserPasswordById(userId, pwhash, pwsalt);
            return JsonConvert.SerializeObject(update);
        }
    }
}