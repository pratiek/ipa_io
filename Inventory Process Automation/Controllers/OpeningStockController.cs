﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.ERP.ERPSpecifics.SynergyERP;
using IPA.Lib.Models;
using IPA.Lib.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpeningStockController : ControllerBase
    {
        [HttpOptions("OpeningStockTemplate")]
        public string openingStockTemplate()
        {
            return "";
        }
        [Authorize]
        [HttpGet("OpeningStockTemplate")]
        public String OpeningStockTemplate(string docIdentifier)
        {
            List<dynamic> templates = OpeningStock.GetOpeningStock(docIdentifier);
            return JsonConvert.SerializeObject(templates);
        }
        [HttpOptions("getItemStatus")]
        public string getItemStatus()
        {
            return "";
        }
        [Authorize]
        [HttpGet("getItemStatus")]
        public String GetItemStatus(string docIdentifier, string itemCode)
        {

            var expConverter = new ExpandoObjectConverter();
            var docIdentifierObj = JsonConvert.DeserializeObject<ExpandoObject>(docIdentifier.ToString(), expConverter);


            var status = MovementUtil.GetMovementCount(docIdentifierObj, MOVEMENT_DIRECTION.INWARDS, itemCode);
            return status.ToString();
        }
    }
}