﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using IPA.Lib.Core;
using IPA.Lib.Core.Barcoding;
using Microsoft.AspNetCore.Authorization;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models.ParameterModels;
using System.Drawing;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BarcodeController : ControllerBase
    {
        //provides printing configurations available in the system
        [HttpOptions("PrintConfig")]
        public string printConfig()
        {
            return "";
        }
        [Authorize]
        [HttpGet("PrintConfig")]
        public String PrintConfig()
        {
            dynamic PrintingConfigs = new ExpandoObject();
            //PrintingConfigs.AvailablePrinters = Printing.GetPrinters();
            PrintingConfigs.Layouts = PrintLayout.GetLayouts();
            return JsonConvert.SerializeObject(PrintingConfigs);
        }



        [HttpGet("GetPrinters")]
        public String GetPrinters()
        {
            var Printers = Printing.GetPrinters();
            return JsonConvert.SerializeObject(Printers);
        }

        [HttpOptions("Print")]
        public string PrintBarcodesOPT()
        {
            return "";
        }

        [Authorize]
        [HttpPost("Print")]
        public string PrintBarcodes(dynamic fieldData)
        {
            var expConverter = new ExpandoObjectConverter();
            var PrintParameters = JsonConvert.DeserializeObject<ExpandoObject>(fieldData.ToString(), expConverter);

            var PrintingConfig = PrintParameters.printDetails;

            PrintInstance pI = new PrintInstance
            {
                ItemCode = PrintingConfig.itemCode,
                ItemDesc = PrintingConfig.itemDesc,
                DateParam = PrintingConfig.date,
                PrintQuantity = PrintingConfig.quantity,
                RefDocIdentifier = JsonConvert.SerializeObject(PrintParameters.DocumentIdentifier),
                HasSubItem = PrintingConfig.subComponentData.hasSubComponent,
                SubItemCount = PrintingConfig.subComponentData.hasSubComponent ? Convert.ToInt64(PrintingConfig.subComponentData.data) : null,
                PrintlayoutId = PrintingConfig.printLayout.LayoutId
            };

            pI.PrintlayoutId = PrintingConfig.printLayout.LayoutId;
            List<ItemBarcode> BarcodesToPrint = ItemBarcoding.CreateBarcodeSeries(pI);

            return JsonConvert.SerializeObject(pI);

            /*var PrintMode = PrintFormats.LAYOUT_A5;
            switch (PrintingConfig.printMode)
            {
                case "1":
                    PrintMode = PrintFormats.LAYOUT_A5;
                    break;

                case "2":
                    PrintMode = PrintFormats.LAYOUT_THERMAL;
                    break;

                default:
                    PrintMode = PrintFormats.LAYOUT_A5;
                    break;

            }*/

           
            //return JsonConvert.SerializeObject(BarcodesToPrint);

        }


        [AllowAnonymous]
        [HttpOptions("GetBarcodeHTML")]
        public string GetBarcodeHTMLOPT()
        {
            return "";
        }

        [HttpGet("GetBarcodeHTML")]
        public ContentResult GetBarcodeHTML(int instanceId)
        {
            PrintInstance pI = PrintInstance.GetInstanceById(instanceId);

            List<ItemBarcode> BarcodesToPrint = PrintInstance.GetBarcodesForInstance(instanceId);
            List<BarcodeCreationParameter> barcodeStrings = BarcodesToPrint.ConvertAll<BarcodeCreationParameter>(ibc => new BarcodeCreationParameter
            {
                Code = ibc.Barcode,
                Label = ibc.PrintInstance.ItemDesc
            });

            string html = BarcodeCreation.renderAsTextHTML(barcodeStrings, PrintLayout.GetLayoutById(pI.PrintlayoutId));


            var response = new HttpResponseMessage();
            response.Content = new StringContent(html);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            //return response;
            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = html
            };

        }




        [HttpOptions("ClassifyBarcodes")]
        public string ClassifyBarcodesOPT()
        {
            return "";
        }

        [Authorize]
        [HttpPost("ClassifyBarcodes")]
        public string ClassifyBarcodes(dynamic fieldData)
        {
            var InputBarcodes = JsonConvert.DeserializeObject<List<string>>(fieldData.ToString());

            return JsonConvert.SerializeObject(BarcodeID.ClassifyBarcodes(InputBarcodes));
        }
        [AllowAnonymous]
        [HttpOptions("PrintedBarcodeList")]
        public string printedBarcodeList()
        {
            return "";
        }
        [Authorize]
        [HttpGet("PrintedBarcodeList")]
        public List<ItemBarcode> PrintedBarcodeList(string itemCode)
        {
            List<PrintInstance> printInstances = DBManager.SelectObjects<PrintInstance>("select InstanceId from dbo.PRINT_INSTANCE WHERE ItemCode ="+itemCode+";");
            var combinedString = String.Join(",", (from a in printInstances select a.InstanceId));
            List<ItemBarcode> barcodes = DBManager.SelectObjects<ItemBarcode>("select * from dbo.BARCODE_LOG WHERE PrintInstanceId in ("+combinedString+");");
            return barcodes;
        }


        [HttpGet("TestPrinting")]
        public string DocumentItems(string barcodeText, int LayoutId)//string VoucherNo, string FormCode, string DocumentType)
        {
            BarcodeCreation.PrintSeries(new List<BarcodeCreationParameter> { new BarcodeCreationParameter {
                Code = barcodeText
            } }, PrintLayout.GetLayoutById(LayoutId));
            return "done";

        }

    }
}
