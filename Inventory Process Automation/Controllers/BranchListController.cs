﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.ERP.ERPSpecifics.SynergyERP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchListController : Controller
    {
        [HttpOptions("CompanyList")]
        public string companyList()
        {
            return "";
        }

        [Authorize]
        [HttpGet("CompanyList")]
        public String CompanyLists()
        {
            dynamic CompanyLists = new ExpandoObject();
            CompanyLists = CompanyList.GetCompanyList();
            return JsonConvert.SerializeObject(CompanyLists);
        }

        [HttpOptions("BranchList")]
        public string branchList()
        {
            return "";
        }

        [Authorize]
        [HttpGet("BranchList")]
        public string BranchLists(string companyCode)
        {
            dynamic BranchLists = new ExpandoObject();
            BranchLists = BranchList.GetBranchList(companyCode);
            return JsonConvert.SerializeObject(BranchLists);
        }


    }
}