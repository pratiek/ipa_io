﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;

using IPA.Lib.Core;
using IPA.Lib.Models;
using IPA.Lib.Core.Database;
using IPA.Lib.ERP.StandardObjects;
using System.Data;
using System.Dynamic;
using Microsoft.AspNetCore.Authorization;
using IPA.Lib.Core.Authentication;

namespace IPA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GRNController : ControllerBase
    {
        // GET: api/Data

        [Authorize]
        [HttpGet]
        public string Get(string DataToCode)
        {
            //BarcodeCreation.PrintSeries(new List<string> { DataToCode }, PrintFormats.LAYOUT_THERMAL);
            return JsonConvert.SerializeObject(Printing.GetPrinters());
        }

        //gets the list of GRN documents in the system
        [HttpOptions("Documents")]
        public string documents()
        {
            return "";
        }
        [Authorize]
        [HttpGet("Documents")]
        public string Documents()
        {
            dynamic Filters = new ExpandoObject();
            Filters.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
            Filters.BranchCode = AuthManager.GetCurrentUser().BranchCode;

            List<Document> receiptItems = GoodsReceipt.GetDocuments(Filters);
            return JsonConvert.SerializeObject(receiptItems);   
        }


        [HttpOptions("DocumentItems")]
        public string documentItems()
        {
            return "";
        }


        [Authorize]
        [HttpGet("DocumentItems")]
        public string DocumentItems(string VoucherNo, string FormCode)
        {
            dynamic Filters = new ExpandoObject();
            Filters.VoucherNo = VoucherNo;
            Filters.FormCode = FormCode;
            Filters.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
            Filters.BranchCode = AuthManager.GetCurrentUser().BranchCode;

            List<DocumentItem> receiptItems = GoodsReceipt.GetDocumentItems(Filters);
            return JsonConvert.SerializeObject(receiptItems);

        }


        // POST: api/Data
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Data/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
