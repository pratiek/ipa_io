﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Reports;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        [HttpOptions("locationStatusReport")]
        public string locationReport()
        {
            return "";
        }

        [Authorize]
        [HttpGet("locationStatusReport")]
        public String LocationReport(int ERPLocationId,string itemCode)
        {
            dynamic reportList = new ExpandoObject();
            reportList = LocationStatus.getLocations(ERPLocationId,itemCode);
            return JsonConvert.SerializeObject(reportList);
        }

        [HttpOptions("GetStoredBarcodes")]
        public string GetStoredBarcodes()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetStoredBarcodes")]
        public List<dynamic> GetStoredBarcodes(int ERPLocationId, string itemCode)
        {
            dynamic reportList = new ExpandoObject();
            reportList = LocationStatus.getLocationsItems(ERPLocationId,itemCode);
            return reportList;
        }

        [HttpOptions("GetItemsFromErpLocation")]
        public string GetItemsFromErpLocation()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetItemsFromErpLocation")]
        public List<dynamic> GetItemsFromErpLocation(int ERPLocationId)
        {
            dynamic reportList = new ExpandoObject();
            reportList = LocationStatus.getItemsFromErpLocation(ERPLocationId);
            return reportList;
        }

        [HttpOptions("GetAgeingReport")]
        public string GetAgeingReport()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetAgeingReport")]
        public String GetAgeingReport(int ERPLocationId, string itemCode)
        {
            dynamic reportList = new ExpandoObject();
            reportList = LocationStatus.getAgeingReport(ERPLocationId, itemCode);
            return JsonConvert.SerializeObject(reportList);
        }

        [HttpOptions("GetBarCodeForAgeing")]
        public string GetBarCodeForAgeing()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetBarCodeForAgeing")]
        public List<dynamic> GetBarCodeForAgeing(int ERPLocationId, string itemCode,string storageDate)
        {
            dynamic reportList = new ExpandoObject();
            reportList = LocationStatus.GetBarCodeForAgeing(ERPLocationId, itemCode,storageDate);
            return reportList;
        }



    }
}