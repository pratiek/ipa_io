﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using IPA.Lib.Core.Barcoding;
using IPA.Lib.Metrics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        [HttpOptions("DashboardStats")]
        public string dashboardStats()
        {
            return "";
        }

        [Authorize]
        [HttpGet("DashboardStats")]
        public String DashboardStats()
        {
            dynamic DashboardStats = new ExpandoObject();
            DashboardStats.barcodesStored = DashboardMetrics.GetBarcodeStored();
            DashboardStats.barcodesShipped = DashboardMetrics.GetBarcodeShipped();
            DashboardStats.locationOccupied = DashboardMetrics.GetLocationOccupied();
            DashboardStats.locationFreed = DashboardMetrics.GetLocationFreed();
            DashboardStats.ItemsMoved = DashboardMetrics.getItemMovementWeekIn();
            DashboardStats.ItemsMovedOut = DashboardMetrics.getItemMovementWeekOut();
            return JsonConvert.SerializeObject(DashboardStats);
        }

    }
}