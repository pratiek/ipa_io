﻿using IPA.Lib.Core;
using IPA.Lib.Core.Authentication;
using IPA.Lib.Core.Barcoding;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.ERP.StandardObjects;
using IPA.Lib.Models;
using IPA.Lib.Models.ParameterModels;
using IPA.Lib.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Inventory_Process_Automation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        [HttpOptions("CreateLocations")]
        public string CreateLocationsOPT()
        {
            return "";
        }

        [Authorize]
        [HttpPost("CreateLocations")]
        public string CreateLocations(LocationEnumerationParameters creationParams)
        {
            var locations = LocationManagement.CreateLocations(creationParams);
            return JsonConvert.SerializeObject(locations);
        }



        [HttpOptions("PrintLocations")]
        public string PrintLocationsOPT()
        {
            return "";
        }

        //[Authorize]
        [HttpGet("PrintLocations")]
        public ContentResult PrintLocations(string locParams)
        {

            dynamic enumParams = JsonConvert.DeserializeObject<dynamic>(locParams);
            List<StorageLocation> locationsToPrint = LocationManagement.GetLocations(enumParams.ToObject<LocationEnumerationParameters>());

            List<BarcodeCreationParameter> barcodeStrings = locationsToPrint.ConvertAll<BarcodeCreationParameter>(loc => new BarcodeCreationParameter { Code = loc.Barcode });
            long printMode = enumParams.printMode;
            string html = "";
            if (barcodeStrings.Count > 0) {
                //BarcodeCreation.PrintSeries(barcodeStrings, PrintLayout.GetLayoutById(printMode));
                html = BarcodeCreation.renderAsTextHTML(barcodeStrings, PrintLayout.GetLayoutById(printMode));

            }

            //return JsonConvert.SerializeObject(locationsToPrint);
            var response = new HttpResponseMessage();
            response.Content = new StringContent(html);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            //return response;
            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = html
            };


        }


        [HttpOptions("StoreItems")]
        public string StoreItemsOPT()
        {
            return "";
        }

        [Authorize]
        [HttpPost("StoreItems")]
        public string StoreItems(dynamic storageData) {

            InventoryActivity iap = JsonConvert.DeserializeObject<InventoryActivity>(storageData.ToString());
            iap.ActivityType = InventoryActivityTypes.STORAGE;
            iap.Direction = MOVEMENT_DIRECTION.INWARDS;
            
            LocationManagement.StoreActivity(iap);

            return JsonConvert.SerializeObject(iap);
        }


        [HttpOptions("ReleaseItems")]
        public string ReleaseItemsOPT()
        {
            return "";
        }


        [Authorize]
        [HttpPost("ReleaseItems")]
        public string ReleaseItems(dynamic storageData)
        {
            InventoryActivity iap = JsonConvert.DeserializeObject<InventoryActivity>(storageData.ToString());
            iap.ActivityType = InventoryActivityTypes.RELEASE;
            iap.Direction = MOVEMENT_DIRECTION.OUTWARDS;

            LocationManagement.StoreActivity(iap);

            return JsonConvert.SerializeObject(iap);
        }

        [HttpOptions("FindItems")]
        public string FindItemsOPT()
        {
            return "";
        }


        [Authorize]
        [HttpPost("FindItems")]
        public string FindItems(dynamic filters){
            ItemSearchParameters isp = JsonConvert.DeserializeObject<ItemSearchParameters>(filters.ToString());
            Dictionary<string, List<ItemLocationSearchResult>> results= LocationManagement.FindItems(isp);
            
            return JsonConvert.SerializeObject(results);
        }
        [HttpOptions("LocationList")]
        public string locationList()
        {
            return "";
        }

        [Authorize]
        [HttpGet("LocationList")]
        public string UsersList()
        {
            dynamic response = new ExpandoObject();
            List<StorageLocation> locations = DBManager.SelectObjects<StorageLocation>("select * from STORAGE_LOCATION;");
            response = locations;
            return JsonConvert.SerializeObject(response);
        }


        [HttpOptions("ERPLocations")]
        public string ERPLocationsOPT()
        {
            return "";
        }

        [Authorize]
        [HttpGet("ERPLocations")]
        public string ERPLocations()
        {
            dynamic Filters = new ExpandoObject();
            Filters.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
            List<ERPLocation> locations = Warehousing.GetLocations(Filters);
            return JsonConvert.SerializeObject(locations);
        }

        [HttpOptions("AddERPLocation")]
        public string addERPLocation()
        {
            return "";
        }


        [Authorize]
        [HttpPost("AddERPLocation")]
        public string AddERPLocation(dynamic filters)
        {
            ERPLocation el = new ERPLocation
            {
                Identifier = filters.Identifier,
                Name = filters.Name,
                Code = filters.Code
            };

            el.Store();
            
            return JsonConvert.SerializeObject(el);
        }

        [HttpOptions("getERPLocations")]
        public string getERPLocationsOPT()
        {
            return "";
        }

        [Authorize]
        [HttpGet("getERPLocations")]
        public string GetERPLocations()
        {
            dynamic response = new ExpandoObject();
            List<dynamic> locations = DBManager.SelectObjects<dynamic>("select * from ERP_LOCATION;");
            response = locations;
            return JsonConvert.SerializeObject(response);
        }

        [HttpOptions("GetLocationRanges")]
        public string GetLocationRangesOPT()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetLocationRanges")]
        public string GetLocationRanges(int LocationId)
        {
            dynamic response = LocationManagement.GetLocationRanges(LocationId);
            return JsonConvert.SerializeObject(response);
        }


        [HttpOptions("GetAllLocations")]
        public string GetAllLocationsOPT()
        {
            return "";
        }

        [Authorize]
        [HttpGet("GetAllLocations")]
        public string GetAllLocations(int LocationId, string Block, string Side)
        {
            dynamic response = LocationManagement.GetAllLocations(LocationId, Block, Side);
            return JsonConvert.SerializeObject(response);
        }




    }
}