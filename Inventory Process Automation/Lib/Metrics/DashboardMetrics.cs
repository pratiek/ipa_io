﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Metrics
{
    public class DashboardMetrics
    {
        public static int GetBarcodeStored()
        {
            string SQL = "select count(distinct(ItemBarcodeId)) as Count from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where  ia.Direction = 1 and ia.CreationTime >=  ( getdate() - 7) ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;
            return count;
        }
            
        public static int GetBarcodeShipped()
        {
            string SQL = "select count(distinct(ItemBarcodeId)) as Count from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where  ia.Direction = 2 and ia.CreationTime >=  ( getdate() - 7) ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;
            return count;

        }

        public static int GetLocationOccupied()
        {
            string SQL = "select count(distinct(LocationId)) as Count from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where  ia.Direction = 1 and ia.CreationTime >=  ( getdate() - 7) ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;
            return count;
        }

        public static int GetLocationFreed()
        {
            string SQL = "select count(distinct(LocationId)) as Count from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where  ia.Direction = 2 and ia.CreationTime >=  ( getdate() - 7) ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;
            return count;

        }

        public static List<dynamic> getItemMovementWeekIn()
        {
            string SQL = "select concat('Week', '-', datepart(week,im.CreationTime)) as dateRange,count(LocationId) as movementCount from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where ia.Direction = 1 and ia.CreationTime >=  ( getdate() - 70) group by datepart(year,im.CreationTime),datepart(week,im.CreationTime);";
            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL);
            return items;
        }

        public static List<dynamic> getItemMovementWeekOut()
        {
            string SQL = "select concat('Week', '-', datepart(week,im.CreationTime)) as dateRange,count(LocationId) as movementCount from ITEM_MOVEMENT im join INVENTORY_ACTIVITY ia on im.ActivityId = ia.ActivityId where ia.Direction = 2 and ia.CreationTime >=  ( getdate() - 70) group by datepart(year,im.CreationTime),datepart(week,im.CreationTime);";
            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL);
            return items;
        }
    }
}
