﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.Net;
using System.Security.Authentication;

public static class ExceptionMiddleware
{
    public static void ConfigureExceptionHandler(this IApplicationBuilder app)
    {
        app.UseExceptionHandler(appError =>
        {
            appError.Run(async context =>
            {
                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (contextFeature != null)
                {
                    context.Response.StatusCode = (int)GetErrorCode(contextFeature.Error);
                    APIError errorObj = new APIError {
                        Message = contextFeature.Error.Message
                    };
                    
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(errorObj));
                }
            });
        });
    }
    private static HttpStatusCode GetErrorCode(Exception e)
    {
        switch (e)
        {
            case ValidationException _:
                return HttpStatusCode.BadRequest;
            case AuthenticationException _:
                return HttpStatusCode.Forbidden;
            case NotImplementedException _:
                return HttpStatusCode.NotImplemented;
            default:
                return HttpStatusCode.InternalServerError;
        }
    }
}