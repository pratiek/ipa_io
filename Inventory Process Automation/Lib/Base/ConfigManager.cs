﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Base
{
    public class ConfigManager
    {
        public static string getAppSettings(String settingName)
        {
            return Startup.appConfiguration[settingName];
        }
    }
}
