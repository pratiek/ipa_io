﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using NLog;
using Newtonsoft.Json;

namespace IPA.Lib.Base
{
    public class Logging
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static void AddLog(string message) {
            logger.Debug(message);
            Console.WriteLine("LOGGING : " + message);
        }

        public static void DumpObject(object ObjToDump) {
            Console.WriteLine(JsonConvert.SerializeObject(ObjToDump));
            logger.Debug(JsonConvert.SerializeObject(ObjToDump));
        }

    }
}
