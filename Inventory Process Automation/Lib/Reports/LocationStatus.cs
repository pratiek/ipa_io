﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Reports
{
    public class LocationStatus
    {
        public static List<dynamic> getLocations(int ERPLocationId,string itemCode)
        {

            Dictionary<string, object> args = new Dictionary<string, object>();
            string SQL = "";
            if (string.IsNullOrEmpty(itemCode))
            {
                args.Add("Aisle", ERPLocationId);
                SQL = "SELECT loc.LocationId,case when sum(CASE WHEN mov.IsActive = 1 THEN 1 ELSE 0 END ) > 0 then 'Occupied' else 'Empty' end AS 'Status' ,  sum(CASE WHEN mov.IsActive = 1 THEN 1 ELSE 0 END ) AS 'ItemCount' , min(loc.Barcode) as Barcode,max(mov.CreationTime) as last_updated FROM STORAGE_LOCATION loc LEFT JOIN ITEM_MOVEMENT mov ON loc.LocationId = mov.LocationId WHERE loc.Aisle = @Aisle group by loc.LocationId";
            }
            else
            {
                args.Add("Aisle", ERPLocationId);
                args.Add("ItemCode", itemCode);
                SQL = @"SELECT T0.LocationId,case when sum(CASE WHEN T1.IsActive = 1 THEN 1 ELSE 0 END ) > 0 then 'Occupied' else 'Empty' end AS 'Status'
                        ,sum(CASE WHEN T1.IsActive = 1 THEN 1 ELSE 0 END) AS 'ItemCount'
                        ,min(T0.Barcode) as Barcode,
                         max(T1.CreationTime) as last_updated
                         FROM STORAGE_LOCATION T0
                         inner join ITEM_MOVEMENT T1
                         ON T0.LocationId = T1.LocationId
                         inner join BARCODE_LOG T2
                         on T2.BarcodeID = T1.ItemBarcodeId
                         inner join PRINT_INSTANCE T3
                         on T3.InstanceId = T2.PrintInstanceId
                         WHERE T0.Aisle = @Aisle and T3.ItemCode = @ItemCode and T1.IsActive = '1' group by T0.LocationId";
            }
                List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL, args);

            return items;
        }

        public static List<dynamic> getLocationsItems(int ERPLocationId,string ItemCode)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            string SQL = "";
            if(!string.IsNullOrEmpty(ItemCode))
            {
                args.Add("LocationId", ERPLocationId);
                args.Add("ItemCode", ItemCode);
                SQL = "select T0.Barcode,T2.ItemDesc from BARCODE_LOG T0 right join (select LocationId,ItemBarcodeId from ITEM_MOVEMENT where LocationId=@LocationId and IsActive='1') as T1 on  T0.BarcodeID=T1.ItemBarcodeId left join (select instanceId,itemDesc,ItemCode from PRINT_INSTANCE) as T2 on T0.PrintInstanceId=T2.InstanceId where T2.ItemCode=@ItemCode";
            }
            else
            {
                args.Add("LocationId", ERPLocationId);
                SQL = "select T0.Barcode,T2.ItemDesc from BARCODE_LOG T0 right join (select LocationId,ItemBarcodeId from ITEM_MOVEMENT where LocationId=@LocationId and IsActive='1') as T1 on  T0.BarcodeID=T1.ItemBarcodeId left join (select instanceId,itemDesc from PRINT_INSTANCE) as T2 on T0.PrintInstanceId=T2.InstanceId";
            }

            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL, args);
            return items;
        }


        public static List<dynamic> getItemsFromErpLocation(int ERPLocationId)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Aisle", ERPLocationId);
            string SQL = @"select Distinct T2.ItemCode,T2.ItemDesc from BARCODE_LOG T0
                             inner join ITEM_MOVEMENT T1
                             on
                             T0.BarcodeID = T1.ItemBarcodeId
                             inner join PRINT_INSTANCE T2
                             on
                             T0.PrintInstanceId = T2.InstanceId
                             inner join STORAGE_LOCATION T3
                             on
                             T3.LocationId = T1.LocationId
                              where
                             T3.Aisle = @Aisle and T1.IsActive = '1'";
            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL, args);
            return items;
        }


        public static List<dynamic> getAgeingReport(int ERPLocationId, string itemCode)
        {

            Dictionary<string, object> args = new Dictionary<string, object>();
            string SQL = "";
            if (string.IsNullOrEmpty(itemCode))
            {
                args.Add("Aisle", ERPLocationId);
                SQL = @"SELECT T0.LocationId,
                        'Occupied' as Status,
                        sum(CASE WHEN T1.IsActive = 1 THEN 1 ELSE 0 END) AS 'ItemCount'
                        ,T0.Barcode as Barcode,
                         CAST(T1.CreationTime AS DATE) as storage_date
                         FROM STORAGE_LOCATION T0
                         inner join ITEM_MOVEMENT T1
                         ON T0.LocationId = T1.LocationId
                         inner join BARCODE_LOG T2
                         on T2.BarcodeID = T1.ItemBarcodeId
                         inner join PRINT_INSTANCE T3
                         on T3.InstanceId = T2.PrintInstanceId
                         WHERE T0.Aisle = @Aisle and T1.IsActive = '1' and
                         T1.CreationTime is not null
                         group by CAST(T1.CreationTime AS DATE),T0.Barcode,T0.LocationId
                         order by CAST(T1.CreationTime AS DATE) asc";
            }
            else
            {
                args.Add("Aisle", ERPLocationId);
                args.Add("ItemCode", itemCode);
                SQL = @"SELECT T0.LocationId,
                        'Occupied' as Status,
                         T3.ItemCode,
                        sum(CASE WHEN T1.IsActive = 1 THEN 1 ELSE 0 END) AS 'ItemCount'
                        ,T0.Barcode as Barcode,
                         CAST(T1.CreationTime AS DATE) as storage_date
                         FROM STORAGE_LOCATION T0
                         inner join ITEM_MOVEMENT T1
                         ON T0.LocationId = T1.LocationId
                         inner join BARCODE_LOG T2
                         on T2.BarcodeID = T1.ItemBarcodeId
                         inner join PRINT_INSTANCE T3
                         on T3.InstanceId = T2.PrintInstanceId
                         WHERE T0.Aisle = @Aisle and T3.ItemCode = @ItemCode and T1.IsActive = '1' and
                         T1.CreationTime is not null
                         group by CAST(T1.CreationTime AS DATE),T0.Barcode,T0.LocationId,T3.ItemCode
                         order by CAST(T1.CreationTime AS DATE) asc";

            }
            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL, args);

            return items;
        }

        public static List<dynamic> GetBarCodeForAgeing(int ERPLocationId, string ItemCode,string StorageDate)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            string SQL = "";
            if (!string.IsNullOrEmpty(ItemCode))
            {
                args.Add("LocationId", ERPLocationId);
                args.Add("ItemCode", ItemCode);
                args.Add("StorageDate", StorageDate.Split("T")[0]);

                SQL = @"SELECT 
                         T3.ItemDesc,
						 T2.Barcode,
                         CAST(T1.CreationTime AS DATE) as storage_date
                         from ITEM_MOVEMENT T1
                         inner join BARCODE_LOG T2
                         on T2.BarcodeID = T1.ItemBarcodeId
                         inner join PRINT_INSTANCE T3
                         on T3.InstanceId = T2.PrintInstanceId
                         WHERE  T3.ItemCode =@ItemCode  and T1.LocationId = @LocationId and T1.IsActive = '1' and
                         Cast(T1.CreationTime As DATE) = @StorageDate
                         order by CAST(T1.CreationTime AS DATE) asc";
            }

            else
            {
                args.Add("LocationId", ERPLocationId);
                args.Add("StorageDate", StorageDate);
                SQL = @"SELECT 
                         T3.ItemDesc,
						 T2.Barcode,
                         CAST(T1.CreationTime AS DATE) as storage_date
                         from ITEM_MOVEMENT T1
                         inner join BARCODE_LOG T2
                         on T2.BarcodeID = T1.ItemBarcodeId
                         inner join PRINT_INSTANCE T3
                         on T3.InstanceId = T2.PrintInstanceId
                         WHERE  T1.LocationId = @LocationId and T1.IsActive = '1' and
                         Cast(T1.CreationTime As DATE) = @StorageDate
                         order by CAST(T1.CreationTime AS DATE) asc";
            }

            List<dynamic> items = DBManager.SelectObjects<dynamic>(SQL, args);
            return items;
        }

    }
}
