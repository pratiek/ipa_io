﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class InventoryActivity
    {
        public long? ActivityId { get; set; }
        public InventoryActivityTypes ActivityType { get; set; }
        public MOVEMENT_DIRECTION Direction { get; set; }
        public string RefDocIdentifier { get; set; } //json to identify the source document
        public long MovementQty { get; set; }


        public int? RestrictedWarehouseId;
        public ERPLocation RestrictedWarehouse;

        public List<ItemMovementPair> ItemsMoved;

        public long Store()
        {
            string iaInsertSql = "INSERT INTO INVENTORY_ACTIVITY(" +
                "RefDocIdentifier, " +
                "ActivityType, " +
                "Direction, " +
                "MovementQty " +
            ") " +
            " output inserted.ActivityId " +
            " Values (" +
                "@RefDocIdentifier, " +
                "@ActivityType, " +
                "@ActivityType, " +
                "@MovementQty " +
            "); ";

            dynamic result = DBManager.InsertObject<InventoryActivity>(iaInsertSql, this);
            long activityId = Enumerable.FirstOrDefault(result).ActivityId; ;

            this.ActivityId = activityId;

            foreach (ItemMovementPair imp in this.ItemsMoved)
            {
                imp.ActivityId = this.ActivityId;
                imp.Store();
            }


            return activityId;
        }

    }

    public enum InventoryActivityTypes {
        STORAGE = 1,  //storage activity
        RELEASE = 2, //release activity
        TRANSFER_OUT = 3,
        TRANSFER_IN = 4,
    }

    public enum MOVEMENT_DIRECTION
    {
        INWARDS = 1,
        OUTWARDS = 2,
    }
}
