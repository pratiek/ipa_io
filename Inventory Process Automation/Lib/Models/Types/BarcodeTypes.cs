﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.Types
{
    public class BarcodeTypes
    {
        public const string INVALID = "INVALID";
        public const string LOCATION = "LOCATION";
        public const string ITEM = "ITEM";
    }
}
