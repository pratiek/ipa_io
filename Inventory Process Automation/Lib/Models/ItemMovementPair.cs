﻿using IPA.Lib.Core.Barcoding;
using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class ItemMovementPair
    {
        public long? MovementId { get; set; }
        public long? ActivityId { get; set; }
        public string ItemBarcode { get; set; }
        public string LocationBarcode { get; set; }

        public long? ItemBarcodeId {
            get {
                ItemBarcode ibc = BarcodeID.FindItemDetails(ItemBarcode);
                return ibc.BarcodeID;
            }
        }

        public long? LocationId
        {
            get
            {
                if (LocationBarcode != null)
                {
                    StorageLocation lbc = BarcodeID.FindLocationDetails(LocationBarcode);
                    return lbc.LocationId;
                }

                return null;
                
            }
        }

        public long Store() {
            ReleaseItemFromOtherLocations();

            string imInsertSql = "INSERT INTO ITEM_MOVEMENT(" +
                "ActivityId, " +
                "LocationId, " +
                "ItemBarcodeId " +
            ") " +
            " output inserted.MovementId " +
            " Values (" +
                "@ActivityId, " +
                "@LocationId, " +
                "@ItemBarcodeId " +
            "); ";

            dynamic result = DBManager.InsertObject<ItemMovementPair>(imInsertSql, this);
            long movementId = Enumerable.FirstOrDefault(result).MovementId; ;

            this.MovementId = movementId;

            return movementId;
        }

        //free the item and location from movement history to free them
        public void ReleaseItemFromOtherLocations() {
            string setInactiveSql = "UPDATE ITEM_MOVEMENT " +
                "SET ISACTIVE = 0 " +
                "WHERE (ItemBarcodeId = @ItemBarcodeId " +
                //"or LocationId = @LocationId" +
                ") " +
                "and IsActive = @IsActive";

            Dictionary<string, object> args = new Dictionary<string, object>();

            args.Add("ItemBarcodeId", ItemBarcodeId);
            //args.Add("LocationId", LocationId);
            args.Add("IsActive", 1);

            DBManager.RunQuery<dynamic>(setInactiveSql, args);
        }
    }
}
