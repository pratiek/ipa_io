﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class PrintingConfiguration
    {
        public string ItemCode;
        public string MonthOfEntry;
        public string YearOfEntry;
        public int ItemCount;
        public string PrintLayoutName;
        public bool HasSubItems;
        public int SubItemCount;
    }
}
