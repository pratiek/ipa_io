﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IPA.Lib.Models
{
    public class ERPLocation
    {
        public long? Id { get; set; }
        public dynamic Identifier { get; set; }

        public string IdentifierString { get {
                return JsonConvert.SerializeObject(Identifier);
         } }

        public string Name { get; set; }
        public string Code { get; set; }


        public void Store()
        {
            string sql = "INSERT INTO ERP_LOCATION(" +
                "Identifier, " +
                "Name, " +
                "Code " +
            ") " +
            " Values (" +
                "@IdentifierString, " +
                "@Name, " +
                "@Code " +
            "); ";
            
            dynamic result = DBManager.InsertObject<ERPLocation>(sql, this);
        }


        public static ERPLocation GetLocationById(int Id) {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Id", Id);

            List<ERPLocation> loc = DBManager.SelectObjects<ERPLocation>("select * from ERP_LOCATION where ID = @Id", args);
            return loc.First();
        }
    }

}
