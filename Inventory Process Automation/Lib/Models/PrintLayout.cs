﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using Newtonsoft.Json;

namespace IPA.Lib.Models
{
    public class PrintLayout
    {
        public long? LayoutId { get; set; }
        public string Name { get; set; }
        //public SizedRect PaperSize;
        public int PaperSizeH { get; set; }
        public int PaperSizeW { get; set; }
        //public SizedRect BarcodeSize  { get; set; }
        public int BarcodeSizeH { get; set; }
        public int BarcodeSizeW { get; set; }
        public int RowsPerPage { get; set; }
        public int ColumnsPerPage { get; set; }
        public DIRECTION BarcodeDirection { get; set; }
        public DIRECTION PageDirection { get; set; }
        public int PaperMarginX { get; set; }
        public int PaperMarginY { get; set; }
        public int BarcodePaddingX { get; set; }
        public int BarcodePaddingY { get; set; }
        public int Barwidth { get; set; }
        public string DefaultPrinter { get; set; }
        public bool IncludeNameLabel { get; set; }
        public bool IncludeBarcodeLabel { get; set; }

        public int PrintResolution { get; set; }

        public static PrintLayout GetLayoutById(long? PrintLayoutId){
            if (PrintLayoutId == default(long)) {
                return null;
            }

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("LayoutId", PrintLayoutId);

            List<PrintLayout> layout = DBManager.SelectObjects<PrintLayout>("select * from PRINT_LAYOUTS where LayoutId = @LayoutId", args);
            return layout.First();
        }


        public static List<PrintLayout> GetLayouts()
        {
            List<PrintLayout> layouts = DBManager.SelectObjects<PrintLayout>("select * from PRINT_LAYOUTS;");
            return layouts;
        }

    }

    public class SizedRect {
        public int Height;
        public int Width;
    }

    public enum DIRECTION {
        VERTICAL = 1,
        HORIZONTAL = 2
    }

    /*public class PrintFormats {
        public static PrintLayout LAYOUT_THERMAL = new PrintLayout {
            Name = "Thermal",
            PaperSizeH = 250,
            PaperSizeW= 450 ,
            BarcodeSizeH = 100,
            BarcodeSizeW = 300,
            BarcodeDirection = DIRECTION.VERTICAL,
            PageDirection = DIRECTION.VERTICAL,
            RowsPerPage = 1,
            ColumnsPerPage = 1,
            PaperMarginX = 0,
            PaperMarginY = 150,
            BarcodePaddingX = 20,
            BarcodePaddingY = 20
        };

        public static PrintLayout LAYOUT_A5 = new PrintLayout
        {
            Name = "A5",
            PaperSizeH = 1200,
            PaperSizeW = 700,
            BarcodeSizeH = 100,
            BarcodeSizeW = 300 ,
            BarcodeDirection = DIRECTION.HORIZONTAL,
            PageDirection = DIRECTION.HORIZONTAL,
            RowsPerPage = 6,
            ColumnsPerPage = 2,
            PaperMarginX = 20,
            PaperMarginY = 20,
            BarcodePaddingX = 20,
            BarcodePaddingY = 20
        };
    }*/
}
