﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class StorageLocation
    {
        public string Type = "Location";

        public long? LocationId { get; set; }

        public long Aisle { get; set; }
        public string Block { get; set; }
        public string Side { get; set; }

        public long Rack { get; set; }
        public long Layer { get; set; }
        public char Partition { get; set; }

        public string Barcode {
            get {
                return Aisle.ToString().PadLeft(2, '0') + "-" + Block.ToString().PadLeft(2, '0') + "-" + Side.PadLeft(2, '0') + "-" + Rack.ToString().PadLeft(2, '0') + "-" + Layer.ToString().PadLeft(2, '0') + "-" + Partition.ToString().PadLeft(2, '0');
            }
        }

        public bool Exists() {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Barcode", this.Barcode);

            List<dynamic> res = DBManager.SelectObjects<dynamic>("select count(*) as count from STORAGE_LOCATION where Barcode = @Barcode", args);
            var counter = res.First().count;

            if (counter > 0) {
                return true;
            }

            return false;
        }

        public long Store()
        {
            string lInsertSql = "INSERT INTO STORAGE_LOCATION(" +
                "Aisle, " +
                "Block, " +
                "Side, " +
                "Rack, " +
                "Layer," +
                "Partition," +
                "Barcode" +
            ") " +
            " output inserted.LocationId " +
            " Values (" +
                "@Aisle, " +
                "@Block, " +
                "@Side, " +
                "@Rack, " +
                "@Layer," +
                "@Partition," +
                "@Barcode" +
            "); ";

            dynamic result = DBManager.InsertObject<StorageLocation>(lInsertSql, this);
            long locationId = Enumerable.FirstOrDefault(result).LocationId;;

            this.LocationId = locationId;

            return locationId;
        }
    }

    public class LocationSides {
        public const string LEFT = "L";
        public const string RIGHT = "R";
    }
}
