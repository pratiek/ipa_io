﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class DocumentItem
    {
        public string Code;
        public string ItemDescription;
        public string Quantity;
        public int PrintCount;
        public List<dynamic> ReleasedItems;
    }
}
