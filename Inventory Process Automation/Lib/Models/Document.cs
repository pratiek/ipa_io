﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class Document
    {
        public string DocumentType;
        public string DocumentDate;
        public DocumentItem[] Items;
        public dynamic DocumentIdentifier;
        public dynamic DocumentData;
    }

    public class DocumentTypes {
        public const string GOODS_RECEIPT = "GOODS_RECEIPT";
        public const string DELIVERY_ORDER = "DELIVERY_ORDER";
        public const string INVENTORY_TRANSFER_OUT = "INVENTORY_TRANSFER_OUT";
        public const string INVENTORY_TRANSFER_IN = "INVENTORY_TRANSFER_IN";
        public const string PURCHASE_RETURN = "PURCHASE_RETURN";
        public const string SALES_RETURN = "SALES_RETURN";
        public const string INTRA_WAREHOUSE_TRANSFER = "INTRA_WAREHOUSE_TRANSFER";
        public const string OPENING_STOCK = "OPENING_STOCK";
    }
}
