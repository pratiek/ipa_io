﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace IPA.Lib.Models
{
    public class ItemBarcode
    {
        public string Type = "Item"; 
        public long? BarcodeID { get; set; }
        private PrintInstance printInstance;
        public PrintInstance PrintInstance {
            get {
                return PrintInstance.GetInstanceById(this.PrintInstanceId);
            }
        }

        public long SerialNumber { get; set; }

        public long? SubItemCode { get; set; }

        public long? PrintInstanceId { get; set; }

        public string Barcode {
            get
            {
                string barcode = "";
                barcode = this.PrintInstance.ItemCode.PadLeft(6, '0') + "-" + this.PrintInstance.DateParam + "-" + Convert.ToString(SerialNumber).PadLeft(6, '0');
                if (SubItemCode != null)
                {
                    barcode = barcode + "." + SubItemCode.ToString().PadLeft(2, '0');
                }

                return barcode;
            }
        }

        public string Label;

        //public PrintFormat PrintFormat;

        public override string ToString() {
            return this.Barcode;
        }
    }
}
