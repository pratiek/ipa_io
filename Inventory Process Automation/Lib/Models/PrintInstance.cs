﻿using IPA.Lib.Core.IPADatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class PrintInstance
    {
        public long? InstanceId { get; set; }

        public string RefDocIdentifier { get; set; }
        public long PrintlayoutId {
            get; set;
        }

        //private PrintLayout _printLayout;
        public PrintLayout PrintLayout {
            get {
                return PrintLayout.GetLayoutById(this.PrintlayoutId);
            }
        }

        public string ItemCode { get; set; }
        public string ItemDesc { get; set; }
        public string DateParam { get; set; }
        public bool HasSubItem { get; set; }
        public long? SubItemCount { get; set; }

        public long PrintQuantity { get; set; }

       
        public static PrintInstance GetInstanceById(long? PrintInstanceId)
        {
            if (PrintInstanceId == default(long))
            {
                return null;
            }

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("PrintInstanceId", PrintInstanceId);

            List<PrintInstance> instances = DBManager.SelectObjects<PrintInstance>("select * from PRINT_INSTANCE where InstanceId = @PrintInstanceId", args);
            return instances.First();
        }

        public static long StorePrintInstance(PrintInstance pInstance)
        {
            string pInsertSql = "INSERT INTO PRINT_INSTANCE(" +
                "RefDocIdentifier," +
                "PrintlayoutId," +
                "ItemCode," +
                "ItemDesc," +
                "DateParam," +
                "HasSubItem," +
                "SubItemCount," +
                "PrintQuantity" +
            ") " +
            " output inserted.InstanceId " +
            " Values (" +
                "@RefDocIdentifier," +
                "@PrintlayoutId," +
                "@ItemCode," +
                "@ItemDesc," +
                "@DateParam," +
                "@HasSubItem," +
                "@SubItemCount," +
                "@PrintQuantity" +
            "); ";

            dynamic result = DBManager.InsertObject<PrintInstance>(pInsertSql, pInstance);
            long instanceId = Enumerable.FirstOrDefault(result).InstanceId;//.First());//.First().InstanceId;

            return instanceId;
        }


        public static List<ItemBarcode> GetBarcodesForInstance(int instanceId) {
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("PrintInstanceId", instanceId);

            string sql = "select * from dbo.BARCODE_LOG WHERE PrintInstanceId = @PrintInstanceId";
            List<ItemBarcode> barcodes = DBManager.SelectObjects<ItemBarcode>(sql, args);
            return barcodes;
        }


    }
}
