﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class Printer
    {
        public string name;
        public string status;
        public string isDefault;
        public string isNetworkPrinter;
    }
}
