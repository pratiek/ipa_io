﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class LocationEnumerationParameters
    {
        public long Aisle;
        public String Block;
        public string Side;
        public RangedParameter<long> Rack;
        public RangedParameter<long> Layer;
        public RangedParameter<char> Partition;
    }

    public class RangedParameter<T> {
        public dynamic From;
        public dynamic To;

        public RangedParameter(T from, T to) {
            From = from;
            To = to;
        }
    }
}
