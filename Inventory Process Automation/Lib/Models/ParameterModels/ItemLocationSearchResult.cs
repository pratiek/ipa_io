﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class ItemLocationSearchResult
    {
        public string ItemCode { get; set; }
        public string ItemDesc { get; set; }

        public string ItemBarcode { get; set; }
        public string LocationBarcode { get; set; }

        public DateTime FirstSeenDate { get; set; }
        public DateTime LastActivityDate { get; set; }

    }
}
