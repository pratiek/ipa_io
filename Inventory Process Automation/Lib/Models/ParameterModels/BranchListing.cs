﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class BranchListing
    {
        public string BranchCode;
        public string BranchName;
        public string CompanyCode;
    }
}
