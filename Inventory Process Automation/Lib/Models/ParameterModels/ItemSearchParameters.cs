﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class ItemSearchParameters
    {
        public List<DocumentItem> ItemsToFind;
    }
}
