﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class CompanyListing
    {
        public string CompanyCode;
        public string CompanyName;
    }
}
