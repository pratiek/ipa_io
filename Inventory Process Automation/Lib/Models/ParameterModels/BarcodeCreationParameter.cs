﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models.ParameterModels
{
    public class BarcodeCreationParameter
    {
        public string Code;
        public string Label;
    }
}
