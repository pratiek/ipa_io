﻿using IPA.Lib.Core.IPADatabase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Models
{
    public class User
    {
        public long UserId { get; set; }
        public String UserName { get; set; }

        [JsonIgnore]
        public String PasswordHash { get; set; }

        [JsonIgnore]
        public String PasswordSalt { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String ContactNumber { get; set; }
        //public DateTime CreationTime { get; set; }
        public String CompanyCode { get; set; }
        public String BranchCode { get; set; }
        public String BranchName { get; set; }
        public String CompanyName { get; set; }
        public USER_ROLES UserRole { get; set; }

        public static User GetUserById(long? UserId)
        {
            if (UserId == default(long))
            {
                return null;
            }

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("UserId", UserId);

            List<User> res = DBManager.SelectObjects<User>("select * from USERS where UserId = @UserId", args);
            return res.First();
        }

        public static dynamic UpdateUserById(long? UserId, User usr)
        {
           
            string setInactiveSql = "UPDATE USERS SET UserRole=@UserRole, ContactNumber = @ContactNumber, CompanyCode = @CompanyCode, BranchCode = @BranchCode, Email = @Email where UserID = @UserId";

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("CompanyCode", usr.CompanyCode);
            args.Add("ContactNumber", usr.ContactNumber );
            args.Add("UserRole", Convert.ToInt64(usr.UserRole));
            args.Add("UserId", UserId);
            args.Add("BranchCode", usr.BranchCode);
            args.Add("Email", usr.Email);

            var res = DBManager.RunQuery<dynamic>(setInactiveSql, args);
            
            return res;
        }

        public static dynamic UpdateUserPasswordById(long? UserId, string newPasswordHash, string newPasswordSalt)
        {
            string setInactiveSql = "UPDATE USERS SET PasswordHash=@newPasswordHash, PasswordSalt = @newPasswordSalt where UserID = @UserId";

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("newPasswordHash", newPasswordHash);
            args.Add("newPasswordSalt", newPasswordSalt);
            args.Add("UserId", UserId);
            var res = DBManager.RunQuery<dynamic>(setInactiveSql, args);
            return res;
        }
    }




    public enum USER_ROLES {
        ROLE_BASIC,
        ROLE_MANGER,
        ROLE_ADMIN
    }
}