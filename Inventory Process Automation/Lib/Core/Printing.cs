﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Printing;
using System.Management;

using IPA.Lib.Models;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace IPA.Lib.Core
{
    public class Printing
    {
        public static List<Printer> GetPrinters() {
            List<Printer> printers = new List<Printer>();
            
            var printerQuery = new ManagementObjectSearcher("SELECT * from Win32_Printer");
            foreach (var printerInfo in printerQuery.Get())
            {
                Printer p = new Printer {
                    name = printerInfo.GetPropertyValue("Name").ToString(),
                    status = printerInfo.GetPropertyValue("Status").ToString(),
                    isDefault = printerInfo.GetPropertyValue("Default").ToString(),
                    isNetworkPrinter = printerInfo.GetPropertyValue("Network").ToString()
                };

                printers.Add(p);
            }

            return printers;
        }


        public static void PrintImageToPrinter(Image img, String printerName, PrintLayout pl) {
            PrintDocument pd = new PrintDocument();
            
            PrinterClass.SetDefaultPrinter(printerName);

            if (PrinterClass.GetDefaultPrinterName() != printerName) {
                throw new Exception("Default printer for layout "+ pl.Name +", "+ printerName +", not available for printing");
            }


            pd.PrintPage += (object sender, PrintPageEventArgs e) =>
            {
                Bitmap bmp = new Bitmap(img.Width, img.Height);

                float tgtWidthInches = (float)pl.PaperSizeW / pl.PrintResolution; //tgtWidthMM / 25.4f;
                float tgtHeightInches = pl.PaperSizeH / pl.PrintResolution; //tgtHeightMM / 25.4f;

                float tgtHeightMM = tgtWidthInches * 25.4f;  //thermal paper size in mm
                float tgtWidthMM = tgtHeightInches * 25.4f;

                float srcWidthPx = img.Width;
                float srcHeightPx = img.Height;
                float dpiX = srcWidthPx / tgtWidthInches;
                float dpiY = srcHeightPx / tgtHeightInches;

                bmp = (Bitmap)img;
                bmp.SetResolution(pl.PrintResolution, pl.PrintResolution);

               
                e.Graphics.PageUnit = GraphicsUnit.Millimeter;
                e.Graphics.DrawImage(img, 0, 0, tgtWidthMM, tgtHeightMM);

                Console.WriteLine("BMPPPPP");
            };

            pd.Print();
        }

    }

    //helper class to set default printer on runtime basis
    public static class PrinterClass
    {
        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Printer);

        public static string GetDefaultPrinterName()
        {
            var query = new ObjectQuery("SELECT * FROM Win32_Printer");
            var searcher = new ManagementObjectSearcher(query);

            foreach (ManagementObject mo in searcher.Get())
            {
                if (((bool?)mo["Default"]) ?? false)
                {
                    return mo["Name"] as string;
                }
            }

            return null;
        }
    }
}
