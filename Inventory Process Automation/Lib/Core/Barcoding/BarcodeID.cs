﻿using IPA.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.Core.IPADatabase;
using System.Text.RegularExpressions;


using IPA.Lib.Models.Types;

namespace IPA.Lib.Core.Barcoding
{
    public class BarcodeID
    {
        public static ItemBarcode FindItemDetails(string BarcodeString) {

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("BarcodeString", BarcodeString);

            var barcodes = DBManager.SelectObjects<ItemBarcode>("SELECT * FROM BARCODE_LOG WHERE BARCODE = @BarcodeString", args);

            barcodes = Enumerable.ToList(barcodes);

            ItemBarcode matched_entry = null;
            if (Enumerable.Count(barcodes) > 0) {
                matched_entry = Enumerable.First(barcodes);
            }

            return matched_entry;
        }

        public static StorageLocation FindLocationDetails(string locationBarcode)
        {

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("BarcodeString", locationBarcode);

            var barcodes = DBManager.SelectObjects<StorageLocation>("SELECT * FROM STORAGE_LOCATION WHERE BARCODE = @BarcodeString", args);

            barcodes = Enumerable.ToList(barcodes);

            StorageLocation matched_entry = null;
            if (Enumerable.Count(barcodes) > 0)
            {
                matched_entry = Enumerable.First(barcodes);
            }

            return matched_entry;
        }


        public static string GetBarcodeType(string BarcodeString) {
            int hyphenCount = BarcodeString.Count(x => x == '-');
            if (hyphenCount == 2)
            {
                return BarcodeTypes.ITEM;
            }
            else if(hyphenCount == 5){
                return BarcodeTypes.LOCATION;
            }
            else
            {
                return BarcodeTypes.INVALID;
            }
            
            
        }

        public static Dictionary<string, dynamic> ClassifyBarcodes(List<string> barcodesToClassify) {
            Dictionary<string, dynamic> barcodesClassified = new Dictionary<string, dynamic>();

            foreach (string barcode in barcodesToClassify) {
                if (barcodesClassified.ContainsKey(barcode))
                    continue;

                if (GetBarcodeType(barcode) == BarcodeTypes.INVALID) {
                    barcodesClassified.Add(barcode, null);
                }
                if (GetBarcodeType(barcode) == BarcodeTypes.LOCATION)
                {
                    barcodesClassified.Add(barcode, FindLocationDetails(barcode));
                }

                if (GetBarcodeType(barcode) == BarcodeTypes.ITEM)
                {
                    barcodesClassified.Add(barcode, FindItemDetails(barcode));
                }
            }

            return barcodesClassified;
        }
    }
}
