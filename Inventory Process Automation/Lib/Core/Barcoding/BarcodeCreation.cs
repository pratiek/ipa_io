﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BarcodeLib;
using BarcodeLib.Symbologies;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

using IPA.Lib.Models;
using IPA.Lib.Base;
//using IPA.Lib.Utils;

//using ZXing;
//using ZXing.Common;
using IPA.Lib.Models.ParameterModels;
using Newtonsoft.Json;

namespace IPA.Lib.Core.Barcoding
{
    public class BarcodeCreation
    {
        private static Image CreateBarcode(BarcodeCreationParameter barcodeToCreate, PrintLayout printFormat)
        {

            if (barcodeToCreate.Code.Length > 30) {
               throw new Exception("Barcode too long to be processed. Please check the parameters");
            }
            
            TYPE encoding = TYPE.CODE128;

            var barcode = new Barcode();
            barcode.IncludeLabel = printFormat.IncludeBarcodeLabel;  //We want to display the encoded text below the barcode image.

            barcode.BarWidth = printFormat.Barwidth;
            Image image = barcode.Encode(encoding, barcodeToCreate.Code, printFormat.BarcodeSizeW, printFormat.BarcodeSizeH);
            //image.Save(@"C:\IPADUMP\thermal_bclib.png", ImageFormat.Png);

            //if a label is defined, include the label
            if (barcodeToCreate.Label != null && printFormat.IncludeNameLabel) {
                var newImage = new Bitmap(image.Width, image.Height + 25);
                //newImage.SetResolution(196);//printFormat.PrintResolution, printFormat.PrintResolution);
                var gr = Graphics.FromImage(newImage);


                StringFormat format = new StringFormat();
                format.LineAlignment = StringAlignment.Center;
                format.Alignment = StringAlignment.Center;
                //print item description on barcode
                gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                gr.DrawString(barcodeToCreate.Label, new Font("Arial", 8, FontStyle.Bold), Brushes.Black,
                        new RectangleF(0, 0, image.Width, 20),
                        format
                );

                gr.DrawImageUnscaled(image, 0, 25);


               
                image = newImage;
            }
           
            if (printFormat.BarcodeDirection == DIRECTION.VERTICAL) {
                image.RotateFlip(RotateFlipType.Rotate90FlipXY);
            }

            return image;
        }

        public static string renderAsImageHTML(List<BarcodeCreationParameter> barcodeStrings, PrintLayout pl) {

            List<Bitmap> pages = BarcodeCreation.PrintSeries(barcodeStrings, pl); //PrintLayout.GetLayoutById(pI.PrintlayoutId));

            String html = "<html><head><style type=\"text / css\"> @page{ size: auto; margin: 5mm 10mm 0mm 10mm; } </style></head><body>";
            foreach (Bitmap page in pages)
            {
                /*float tgtWidthInches = (float)1.5f * pl.PaperSizeW / pl.PrintResolution; //tgtWidthMM / 25.4f;
                float tgtHeightInches = pl.PaperSizeH / pl.PrintResolution; //tgtHeightMM / 25.4f;

                float tgtHeightMM = tgtWidthInches * 25.4f;  //thermal paper size in mm by converting inches to mm
                float tgtWidthMM = tgtHeightInches * 25.4f;

                float srcWidthPx = page.Width;
                float srcHeightPx = page.Height;
                float dpiX = srcWidthPx / tgtWidthInches;
                float dpiY = srcHeightPx / tgtHeightInches;

                //bmp = (Bitmap)img;
                //page.SetResolution(pl.PrintResolution, pl.PrintResolution);

                // Convert the image to byte[]
                

                //Bitmap tgtImg = new Bitmap(srcWidthPx, srcHeightPx);

                Bitmap bmp = new Bitmap(Convert.ToInt32(srcWidthPx), Convert.ToInt32(srcHeightPx));

                Graphics g = Graphics.FromImage(bmp);
                bmp.SetResolution(pl.PrintResolution, pl.PrintResolution);
                
                g.PageUnit = GraphicsUnit.Millimeter;
                g.DrawImage(page, 0, 0, tgtWidthMM, tgtHeightMM);

                */
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                page.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                byte[] imageBytes = stream.ToArray();

                string image_str = $@"<img src='data:image/png;base64," + Convert.ToBase64String(imageBytes) + "' > <br>";
                html += image_str;

            }

            html += "</body></html>";

            return html;
        }

        public static string renderAsTextHTML(List<BarcodeCreationParameter> barcodeObjects, PrintLayout pl)
        {
            String html = "<html>" +
                                "<head>" +
                                        "<style type=\"text/css\"> " +
                                            "@page{ size: auto; margin: 5mm 10mm 0mm 10mm;} " +
                                            ".container { width: 300px; text-align:center; align-self: center;} " +
                                            ".pagebreakContainer { background-color:lightblue; page-break-after: always; } " +
                                            ".company-logo { align-content: center; margin: 0px; width: 214px; height: 55px; } " +
                                        "</style>" +
                                        "<link href=\"https://fonts.googleapis.com/css?family=Libre Barcode 128\" rel='stylesheet'>" +
                                        "<script src=\"https://cdn.jsdelivr.net/jsbarcode/3.3.20/JsBarcode.all.min.js\"></script>" +
                                        "</head>" +
                                "<body>";
            var count = 0;

            List<string> barcodeStrings = new List<string>();
            foreach (BarcodeCreationParameter bc in barcodeObjects)
            {
                barcodeStrings.Add(bc.Code);
                count++;
            }

            html += "<script>" +
                "var barcodes_to_encode = "+ JsonConvert.SerializeObject(barcodeObjects) +";" +
                "for(i=0; i<barcodes_to_encode.length; i++){" +
                     "var docContainer = document.createElement('div');"+
                     "docContainer.className = 'container';" +
                     "var pagebreakContainer = document.createElement('div');" +
                     "pagebreakContainer.className = 'pagebreakContainer';" +
                     "var img = document.createElement('img');"+
                     "img.src = 'https://www.himelectronics.com/Media/him.png'; " +
                     "img.className = 'company-logo'; " +
                     "let svg = document.createElementNS(\"http://www.w3.org/2000/svg\", \"svg\");"+
                     "svg.setAttribute('jsbarcode-format', 'code128');"+
                     "svg.setAttribute('jsbarcode-value', barcodes_to_encode[i].Code);"+
                     "svg.setAttribute('jsbarcode-width', 1);"+
                     "svg.setAttribute('jsbarcode-height', 30);"+
                     "svg.className.baseVal = \"jsbarcode\";"+
                     //"pagebreakContainer.appendChild(img);" +
                     "var barcodeText = document.createElement('p');"+
                     "barcodeText.className = 'barcode-text';"+
                     "barcodeText.innerHTML = barcodes_to_encode[i].Label;"+
                     "pagebreakContainer.appendChild(barcodeText); " +
                     "pagebreakContainer.appendChild(svg); " +
                     "docContainer.appendChild(pagebreakContainer);" +
                     "document.body.appendChild(docContainer);" +
                   "}" +
                   "JsBarcode(\".jsbarcode\").init();" +
                "</script>" +
                "</body></html>";

            return html;
        }



        public static List<Bitmap> PrintSeries(List<BarcodeCreationParameter> BarcodesToPrint, PrintLayout PrintLayout){//ItemBarcode[] BarcodesToPrint, PrintFormat PrintLayout) {
            List<Bitmap> PagesToPrint = new List<Bitmap>();

            List<BarcodeCreationParameter> BarcodeChunk = new List<BarcodeCreationParameter>();
            int it = 0;
            int BarcodesPerPage = PrintLayout.ColumnsPerPage * PrintLayout.RowsPerPage;
            do
            {
                BarcodeChunk.Add(BarcodesToPrint.ElementAt(it));
                it++;
                if (it > 0 && it % BarcodesPerPage == 0 || it == BarcodesToPrint.Count) {
                   Bitmap PageToPrint = FormGrid(BarcodeChunk, PrintLayout);
                   PagesToPrint.Add(PageToPrint);
                   BarcodeChunk.Clear();
                }
            } while (it < BarcodesToPrint.Count);


            /*var printCtr = 0;
            foreach (Bitmap PageToPrint in PagesToPrint) {
                PageToPrint.Save(@"C:\IPADUMP\thermal_" + printCtr+".png", ImageFormat.Png);
                printCtr++;

                if (printCtr < 6) {
                    Printing.PrintImageToPrinter(PageToPrint, PrintLayout.DefaultPrinter, PrintLayout);
                }
            }*/

            return PagesToPrint;
        }


        public static Bitmap FormGrid(List<BarcodeCreationParameter> BarcodeChunk, PrintLayout printFormat) {
            var bitmap = new Bitmap(printFormat.PaperSizeW, printFormat.PaperSizeH);
            bitmap.SetResolution(203, 203);
            if (printFormat.PageDirection == DIRECTION.VERTICAL) {
                bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }

            using (var canvas = Graphics.FromImage(bitmap))
            {
                int gridX = 0, gridY = 0;
                for (int xi = 0; xi < BarcodeChunk.Count; xi++)
                {
                    var barcode_image = CreateBarcode(BarcodeChunk.ElementAt(xi), printFormat);
                    
                    canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    if (xi > 0 && xi % printFormat.ColumnsPerPage == 0)
                    {
                        gridX++;
                        gridY = 0;
                    }
                    
                    int xBegin = gridY * (printFormat.BarcodePaddingY + barcode_image.Width) + printFormat.PaperMarginY;
                    int yBegin = gridX * (printFormat.BarcodePaddingX + barcode_image.Height) + printFormat.PaperMarginX;
                    
                    canvas.DrawImage(
                        barcode_image, 
                        new Rectangle(
                            xBegin,
                            yBegin,
                            barcode_image.Width,
                            barcode_image.Height
                            ),
                        new Rectangle(0, 0, barcode_image.Width, barcode_image.Height),
                        GraphicsUnit.Pixel
                        );


                    gridY++;
                }

                canvas.Save();
            }

            return bitmap;
        }
    }
}
