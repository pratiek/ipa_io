﻿using IPA.Lib.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.Core.IPADatabase;

namespace IPA.Lib.Core.Barcoding
{
    public class ItemBarcoding
    {
        public static List<ItemBarcode> CreateBarcodeSeries(PrintInstance instance) {
            List<ItemBarcode> BarcodesToPrint = new List<ItemBarcode>();
            //instance.PrintlayoutId = 1;//PrintLayout.GetLayoutById(1);
            
            instance.InstanceId = PrintInstance.StorePrintInstance(instance);
            long SerialNoStart = GetNextSerialNo(instance.DateParam, instance.ItemCode);

            for (int xc = 0; xc < instance.PrintQuantity; xc++)
            {
                //helper variable to help with sub items

                long? subItemLooper = 0;
                do
                {
                    ItemBarcode ibc = new ItemBarcode
                    {
                        PrintInstanceId = instance.InstanceId,
                        SerialNumber = xc + SerialNoStart,
                        Label = instance.ItemDesc
                    };

                    if (instance.HasSubItem)
                    {
                        subItemLooper++;
                        ibc.SubItemCode = subItemLooper;
                    }
                    BarcodesToPrint.Add(ibc);

                } while (instance.HasSubItem && subItemLooper < instance.SubItemCount);
                
            }

            StoreBarcodes(BarcodesToPrint);

            return BarcodesToPrint;
        }



        public static long GetNextSerialNo(string DateParam, string ItemCode) {
            string snoGetterSql = "select (isnull(max(serialNumber), 0)+1) as NxtSerialNo " +
                "from " +
                "PRINT_INSTANCE PI left join BARCODE_LOG BL " +
                "on PI.InstanceId = BL.PrintInstanceId " +
                "where PI.DateParam = @DateParam " +
                " and PI.ItemCode = @ItemCode; ";

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("DateParam", DateParam);
            args.Add("ItemCode", ItemCode);

            dynamic result = DBManager.RunQuery<PrintInstance>(snoGetterSql, args);
            long instanceId = Enumerable.FirstOrDefault(result).NxtSerialNo + 1;//.First());//.First().InstanceId;

            return instanceId;
        }

        public static void StoreBarcodes(List<ItemBarcode> BarcodesToStore) {
            string barcodeInsertSql = "INSERT INTO BARCODE_LOG(" +
                "PrintInstanceId," +
                "SerialNumber," +
                "SubItemCode," +
                "Barcode" +
            ") " +
            " output inserted.BarcodeId " +
            " Values (" +
                "@PrintInstanceId," +
                "@SerialNumber," +
                "@SubItemCode," +
                "@Barcode" +
            "); ";

            DBManager.InsertObjects<ItemBarcode>(barcodeInsertSql, BarcodesToStore);
        }

       

    }
}
