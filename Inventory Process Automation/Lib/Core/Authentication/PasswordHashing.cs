﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RandomNumberGenerator;
using System.Security.Cryptography;
using System.Text;

using IPA.Lib.Models;
namespace IPA.Lib.Core.Authentication
{
    public class PasswordHashing
    {
        public HashWithSaltResult HashWithSalt(string password, int saltLength, HashAlgorithm hashAlgo)
        {
            RNG rng = new RNG();
            byte[] saltBytes = rng.GenerateRandomCryptographicBytes(saltLength);
           
            byte[] passwordAsBytes = Encoding.UTF8.GetBytes(password);
            List<byte> passwordWithSaltBytes = new List<byte>();
            passwordWithSaltBytes.AddRange(passwordAsBytes);
            passwordWithSaltBytes.AddRange(saltBytes);
            byte[] digestBytes = hashAlgo.ComputeHash(passwordWithSaltBytes.ToArray());
            byte[] digestBytes2 = hashAlgo.ComputeHash(passwordWithSaltBytes.ToArray());

           
            return new HashWithSaltResult(Convert.ToBase64String(saltBytes), Convert.ToBase64String(digestBytes));
        }


        public string GenerateHashForAuth(string password, string salt, HashAlgorithm hashAlgo) {
            byte[] passwordAsBytes = Encoding.UTF8.GetBytes(password);
            byte[] saltAsBytes = Convert.FromBase64String(salt);

            List<byte> passwordWithSaltBytes = new List<byte>();
            passwordWithSaltBytes.AddRange(passwordAsBytes);
            passwordWithSaltBytes.AddRange(saltAsBytes);

            byte[] digestBytes = hashAlgo.ComputeHash(passwordWithSaltBytes.ToArray());
            return Convert.ToBase64String(digestBytes);
        }
    }
}
