﻿using IPA.Lib.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Core.Authentication
{
    public class AuthManager
    {
        private static User _currentUser;
        private static IHttpContextAccessor _httpContextAccessor;
        public static void init(IHttpContextAccessor  accessor) {
            _httpContextAccessor = accessor;
            //AppContext.Current.Session.Set<string>("user", JsonConvert.SerializeObject(user));
        }

        public static User GetCurrentUser() {
            /*if (_currentUser != null) {
                return _currentUser;
            }*/

            if (_httpContextAccessor.HttpContext != null)
            {
                var identity = (System.Security.Claims.ClaimsIdentity)_httpContextAccessor.HttpContext.User.Identity;
                var userId = identity.Claims.FirstOrDefault(c => c.Type == "UserID").Value;
                return User.GetUserById(Convert.ToInt32(userId));
                //SetUser(Convert.ToInt32(userId));
            }
            return _currentUser;
        }

        /*public static void SetUser(int userId) {
            _currentUser = User.GetUserById(userId);
        }*/


    }
}
