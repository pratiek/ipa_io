﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.Models.ParameterModels;
using IPA.Lib.Models;
using IPA.Lib.Util;
using Newtonsoft.Json;
using System.Dynamic;
using IPA.Lib.Core.Authentication;

namespace IPA.Lib.Core
{
    public class LocationManagement
    {
        public static List<StorageLocation> CreateLocations(LocationEnumerationParameters CreationParamts){
            List<StorageLocation> locationsEnumerated = EnumerateLocations(CreationParamts);

            foreach (StorageLocation sl in locationsEnumerated) {
                if (sl.Exists()) {
                    throw new Exception("Location "+ sl.Barcode+" already exists.");
                }
            }
            
            foreach (StorageLocation sl in locationsEnumerated)
            {
                sl.Store();
            }

            return locationsEnumerated;
        }


        private static List<StorageLocation> EnumerateLocations(LocationEnumerationParameters enumerationParams) {
            List<StorageLocation> locationsCreated = new List<StorageLocation>();
            for (long lRack = enumerationParams.Rack.From; lRack <= enumerationParams.Rack.To; lRack++)
            {
                for (long lLayer = enumerationParams.Layer.From; lLayer <= enumerationParams.Layer.To; lLayer++)
                {
                    for (char lPartition = enumerationParams.Partition.From; lPartition <= enumerationParams.Partition.To; lPartition++)
                    {
                        StorageLocation sl = new StorageLocation
                        {
                            Aisle = enumerationParams.Aisle,
                            Block = enumerationParams.Block,
                            Side = enumerationParams.Side,
                            Rack = lRack,
                            Layer = lLayer,
                            Partition = lPartition
                        };

                        locationsCreated.Add(sl);
                    }
                }
            }

            return locationsCreated;
        }

        public static List<StorageLocation> GetLocations(LocationEnumerationParameters enumerationParams) {
            List<StorageLocation> locationsEnumerated = EnumerateLocations(enumerationParams);
            List<StorageLocation> locationsFound = new List<StorageLocation>();

            foreach (StorageLocation sl in locationsEnumerated)
            {
                if (sl.Exists())
                {
                    locationsFound.Add(sl);
                }
            }

            return locationsFound;
        }



        public static void StoreActivity(InventoryActivity ia) {
            //ensure that the document identifier is defined
            if (ia.RefDocIdentifier == null)
            {
                throw new Exception("Reference document not found.");
            }

            ERPLocation loc;
            if ( ia.RestrictedWarehouseId == null){
                throw new Exception("Location not selected.");
            }
            else {
                loc = ERPLocation.GetLocationById((int)ia.RestrictedWarehouseId);
            }

            ia.RestrictedWarehouse = loc ?? throw new Exception("Warehouse not found");

            dynamic docIdentifier = JsonConvert.DeserializeObject<ExpandoObject>(ia.RefDocIdentifier);
            DocumentUtil.ValidateActivity(docIdentifier, ia);

            ia.MovementQty = (long)ia.ItemsMoved.Count;
            ia.Store();

        }


        public static Dictionary<string, List<ItemLocationSearchResult>> FindItems(ItemSearchParameters isp) {
            Dictionary<string, List<ItemLocationSearchResult>> results = new Dictionary<string, List<ItemLocationSearchResult>>();
            foreach (DocumentItem ditm in isp.ItemsToFind) {
                string searchSql = "select " +
                    "PI.ItemCode, " +
                    "PI.ItemDesc, " +
                    "BL.Barcode as ItemBarcode, " +
                    "SL.Barcode as LocationBarcode, " +
                    "PI.CreationTime as FirstSeenDate, " +
                    "IM.CreationTime as LastActivityDate " +
                    "from " +
                    "ITEM_MOVEMENT IM join INVENTORY_ACTIVITY IA " +
                    "on IM.ActivityId = IA.ActivityId " +
                    "join BARCODE_LOG BL " +
                    "on IM.ItemBarcodeId = BL.BarcodeID " +
                    "join PRINT_INSTANCE PI " +
                    "on BL.PrintInstanceId = PI.InstanceId " +
                    "join STORAGE_LOCATION SL " +
                    "on IM.LocationId = SL.LocationId " +
                    "where IM.IsActive = 1 " +
                    "and IA.Direction = 1 " +
                    "and PI.ItemCode = @ItemCode order by PI.CreationTime asc ;";

                Dictionary<string, object> Args = new Dictionary<string, object>();
                Args.Add("@ItemCode", ditm.Code);

                List<ItemLocationSearchResult> ilsr = IPADatabase.DBManager.SelectObjects<ItemLocationSearchResult>(searchSql, Args);
                results.Add(ditm.Code, ilsr);
            }

            return results;
        }

        public static List<dynamic> GetLocationRanges(int LocationId) {
            string searchSql = $@"select 
                                    Block,
                                    Side, 
                                    Min(Rack) as MinRack,  Max(Rack) as MaxRack,
                                    Min(Layer) as MinLayer,  Max(Layer) as MaxLayer, 
                                    min(Partition) as MinPartition, max(partition) as MaxPartition
                                    from STORAGE_LOCATION 
                                    where Aisle = @LocationId
                                    group by Block,Side";

            Dictionary<string, object> Args = new Dictionary<string, object>();
            Args.Add("@LocationId", LocationId);

            List<dynamic> ilsr = IPADatabase.DBManager.SelectObjects<dynamic>(searchSql, Args);
            return ilsr;
        }

        public static List<dynamic> GetAllLocations(int LocationId, string Block, string Side)
        {
            string searchSql = $@"select 
                                    Rack,
                                    Layer,
                                    Partition,
                                    Barcode
                                    from STORAGE_LOCATION 
                                    where 
                                    Aisle = @LocationId
                                    and Block = @Block
                                    and Side = @Side
                                    order by Rack, Layer, Partition";

            Dictionary<string, object> Args = new Dictionary<string, object>();
            Args.Add("@LocationId", LocationId);
            Args.Add("@Block", Block);
            Args.Add("@Side", Side);

            List<dynamic> ilsr = IPADatabase.DBManager.SelectObjects<dynamic>(searchSql, Args);
            
            return ilsr;
        }

    }
}
