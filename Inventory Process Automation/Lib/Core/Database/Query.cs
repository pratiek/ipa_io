﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Core.Database
{
    public class Query
    {
        
        public static DataTable RunQuery(string Query) {
            var DBManager = new OracleDBManager();
            DataTable dt = new DataTable();

            using (OracleCommand cmd = new OracleCommand()) {
                cmd.Connection = DBManager._con;
                cmd.CommandText = Query;
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }
    }
}
