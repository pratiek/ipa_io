﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.Base;

namespace IPA.Lib.Core.Database
{
    public class OracleDBManager
    {
        public OracleConnection _con;
        private const string connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1})) (CONNECT_DATA=(SERVICE_NAME={2}))); User Id={3};Password={4};";
        private static string OracleDBHost = ConfigManager.getAppSettings("OracleDB:OracleDBHost");
        private static string OracleDBPort = ConfigManager.getAppSettings("OracleDB:OracleDBPort");
        private static string OracleDBServiceName = ConfigManager.getAppSettings("OracleDB:OracleDBServiceName");
        private static string OracleDBUser = ConfigManager.getAppSettings("OracleDB:OracleDBUser");
        private static string OracleDBPassword = ConfigManager.getAppSettings("OracleDB:OracleDBPassword");

        public OracleDBManager()
        {
            InitializeDBConnection();
        }

        ~OracleDBManager()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }

        public static OracleConnection GetConnection()
        {
            var connectionStr = string.Format(connectionString,OracleDBHost, OracleDBPort, OracleDBServiceName , OracleDBUser, OracleDBPassword);
            var connection = new OracleConnection(connectionStr);
            return connection;
        }

        private OracleConnection InitializeDBConnection()
        {
            var _conn = GetConnection();
            _conn.Open();
            this._con = _conn;
            return _conn;
        }

        
    }
}
