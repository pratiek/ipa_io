﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IPA.Lib.Base;
using IPA.Lib.Models;
using Microsoft.Extensions.Configuration;

namespace IPA.Lib.Core.IPADatabase
{
    public class DBManager
    {
        public static string CreateConnectionString() {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = ConfigManager.getAppSettings("IPADB:DataSource");   
            builder.UserID = ConfigManager.getAppSettings("IPADB:UserID");              
            builder.Password = ConfigManager.getAppSettings("IPADB:Password");      
            builder.InitialCatalog = ConfigManager.getAppSettings("IPADB:InitialCatalog");
            return builder.ConnectionString;
        }

        public static dynamic InsertObject<T>(string InsertQuery, T ObjectToInsert) {
            List<T> objList = new List<T>();
            objList.Add(ObjectToInsert);
            return InsertObjects<T>(InsertQuery, objList).First();
        }

        public static List<dynamic> InsertObjects<T>(string InsertQuery, List<T> ObjectsToInsert) {
            List<dynamic> results = new List<dynamic>();
            try
            {
                using (var connection = new SqlConnection(CreateConnectionString()))
                {
                    foreach (T obj in ObjectsToInsert)
                    {
                        var result = connection.Query(InsertQuery, obj).ToList();
                        results.Add(result);
                        //return result;
                       // var ID = affectedRows.Single();
                       // Console.WriteLine(affectedRows);
                    }

                    return results;

                }

            }
            catch (Exception ex) {
                throw ex;
                Console.WriteLine(ex);
                //TODO: add log for errors
            }
            return null;
            
        }




        public static List<T>  SelectObjects<T>(string SelectSql)
        {
            return SelectObjects<T>(SelectSql, null);
        }


        public static List<T> SelectObjects<T>(string SelectSql, Dictionary<string, object> Args)
        {
            List<T> results = new List<T>();
            try
            {
                using (var connection = new SqlConnection(CreateConnectionString()))
                {
                    results = connection.Query<T>(SelectSql, Args).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //TODO : add log entry of the error
            }


            return results;
        }


        public static dynamic RunQuery<T>(string Sql, Dictionary<string, object> Args)
        {
            dynamic results = null;
            try
            {
                using (var connection = new SqlConnection(CreateConnectionString()))
                {
                    results = connection.Query(Sql, Args).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //TODO : add log entry of the error
            }


            return results;
        }

    }
}
