﻿using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Util
{
    public class CommonUtil
    {
        public static bool ObjectHasProperty(dynamic obj, string propertyToCheck) {
            if (obj != null && ((IDictionary<string, object>)obj).ContainsKey(propertyToCheck))
            {
                return true;
            }

            return false;
        }

        public static ERPLocation GetDefaultWarehouse(User user) {
            dynamic LocIdentifier = new ExpandoObject();
            LocIdentifier.LocationCode = "01."+user.BranchCode;
            LocIdentifier.CompanyCode = user.CompanyCode;

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("Identifier", JsonConvert.SerializeObject(LocIdentifier));

            List<ERPLocation> loc = DBManager.SelectObjects<ERPLocation>("select * from ERP_LOCATION where Identifier = @Identifier", args);
            if (loc.Count > 0) {
                return loc.First();
            }

            return null;
        }
    }
}
