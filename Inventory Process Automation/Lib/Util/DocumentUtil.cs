﻿using IPA.Lib.Core.Authentication;
using IPA.Lib.ERP;
using IPA.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.Util
{
    public class DocumentUtil
    {
        public static List<DocumentItem> GetDocumentItems(dynamic docIdentifier) {
            dynamic documentType = TranslateDocumentType(docIdentifier.DocumentType);
            if (documentType != null) {
                if (!CommonUtil.ObjectHasProperty(docIdentifier, "CompanyCode"))
                {
                    docIdentifier.CompanyCode = AuthManager.GetCurrentUser().CompanyCode;
                }

                if (!CommonUtil.ObjectHasProperty(docIdentifier, "BranchCode"))
                {
                    docIdentifier.BranchCode = AuthManager.GetCurrentUser().BranchCode;
                }

                dynamic items = ERPRouter.GetObject( (ObjectNames) documentType )
                                            .GetMethod("GetDocumentItems")
                                            .Invoke(null, new object[] { docIdentifier }); ;
                return items;
            }
            throw new Exception("INVALID DOCUMENT");
        }


        public static dynamic TranslateDocumentType(string documentType) {
            if (documentType == DocumentTypes.GOODS_RECEIPT)
            {
                return ObjectNames.GOODS_RECEIPT;
            }

            if (documentType == DocumentTypes.DELIVERY_ORDER)
            {
                return ObjectNames.DISPATCH_ORDER;
            }

            if (documentType == DocumentTypes.PURCHASE_RETURN)
            {
                return ObjectNames.PURCHASE_RETURN;
            }

            if (documentType == DocumentTypes.SALES_RETURN)
            {
                return ObjectNames.SALES_RETURN;
            }

            if (documentType == DocumentTypes.INVENTORY_TRANSFER_OUT)
            {
                return ObjectNames.INVENTORY_TRANSFER_OUT;
            }


            if (documentType == DocumentTypes.INVENTORY_TRANSFER_IN)
            {
                return ObjectNames.INVENTORY_TRANSFER_IN;
            }

            if (documentType == DocumentTypes.INTRA_WAREHOUSE_TRANSFER) {
                return ObjectNames.INTRA_WAREHOUSE_TRANSFER;
            }

            if (documentType == DocumentTypes.OPENING_STOCK)
            {
                return ObjectNames.OPENING_STOCK;
            }

            return null;
        }

        //checks if certain documents need item level validation.
        public static bool RequiresItemValidation(dynamic DocumentIdentifier) {
            ObjectNames[] freeDocuments = { ObjectNames.OPENING_STOCK, ObjectNames.INTRA_WAREHOUSE_TRANSFER };
            dynamic documentType = TranslateDocumentType(DocumentIdentifier.DocumentType);

            //freeDocuments.Contains((int))
            if (documentType != null && freeDocuments.Any(x => (int)x == (int)documentType)) {
                return false;
            }

            return true;
        }


        public static bool ValidateActivity(dynamic DocIdentifier, InventoryActivity iac) {
            List<ItemMovementPair> movementPairs = iac.ItemsMoved;

            List<string> locationBarcodes = new List<string>();
            List<string> itemBarcodes = new List<string>();

            foreach (ItemMovementPair imp in movementPairs) {
                locationBarcodes.Add(imp.LocationBarcode);
                itemBarcodes.Add(imp.ItemBarcode);
            }

            //check if the barcodes in each pair are valid
            if (! (MovementUtil.AreItemBarcodesValid(itemBarcodes) ) ) {
                throw new Exception("Invalid item barcodes detected.");
            }


            //ensure if the items are getting stored, users are only storing in the restricted warehouse.
            if (iac.Direction == MOVEMENT_DIRECTION.INWARDS) {
                if (!MovementUtil.AreLocationBarcodesValid(locationBarcodes)){
                    throw new Exception("Invalid location barcodes detected.");
                }

                if ( ! MovementUtil.ValidateLocationBarcodes(locationBarcodes, iac.RestrictedWarehouse)) {
                    throw new Exception("Locations outside "+iac.RestrictedWarehouse.Name+" detected.");
                }

            }

            //for dispatch activities, ensure all items are originating from the restricted warehouse
            if (iac.Direction == MOVEMENT_DIRECTION.OUTWARDS) {
                if (!MovementUtil.ValidateItemsAreAtLocation(itemBarcodes, iac.RestrictedWarehouse)) {
                    throw new Exception("Items outside the warehouse detected.");
                }
            }


            //check if document level item validation is needed
            //not necessary for intra warehouse transfers and opening balance 
            if (!RequiresItemValidation(DocIdentifier))
            {
                return true;
            }

            //obtain the items in the document 
            List<DocumentItem> itemsInDoc = DocumentUtil.GetDocumentItems(DocIdentifier);
            //ensure all the barcodes are belonging to the items in the documents.
            if (!MovementUtil.ValidateItemBarcodes(itemBarcodes, itemsInDoc))
            {
                throw new Exception("Invalid barcodes found in activity");
            }

            return true;
        }

    }
}
