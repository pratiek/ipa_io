﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using Newtonsoft.Json;

namespace IPA.Lib.Util
{
    public class MovementUtil
    {
        /*Returns the number of items barcodes moved by referencing the specified document*/
        public static int GetMovementCount(dynamic DocumentIdentifier, MOVEMENT_DIRECTION movDir ,string itemCode) {
            if (CommonUtil.ObjectHasProperty(DocumentIdentifier, "CompanyCode"))
            {
                ((IDictionary<String, Object>)DocumentIdentifier).Remove("CompanyCode");
            }

            if (CommonUtil.ObjectHasProperty(DocumentIdentifier, "BranchCode"))
            {
                ((IDictionary<String, Object>)DocumentIdentifier).Remove("BranchCode");
            }

            string SQL = "Select count(bl.Barcode) as Count from INVENTORY_ACTIVITY ia join ITEM_MOVEMENT im on ia.ActivityId = im.ActivityId left join BARCODE_LOG bl on im.ItemBarcodeId = bl.BarcodeID left join PRINT_INSTANCE pinst on pinst.InstanceId = bl.PrintInstanceId where ia.RefDocIdentifier = '" + JsonConvert.SerializeObject(DocumentIdentifier) + "' and ia.Direction = "+ (int)movDir + " AND pinst.ItemCode ="+ itemCode;
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;
            return count;
        }

        /*Returns the list of items barcodes moved by referencing the specified document*/
        public static List<dynamic> GetMovedItems(dynamic DocumentIdentifier, MOVEMENT_DIRECTION movDir, string itemCode)
        {
            if (CommonUtil.ObjectHasProperty(DocumentIdentifier, "CompanyCode")) {
                ((IDictionary<String, Object>)DocumentIdentifier).Remove("CompanyCode");
            }

            if (CommonUtil.ObjectHasProperty(DocumentIdentifier, "BranchCode"))
            {
                ((IDictionary<String, Object>)DocumentIdentifier).Remove("BranchCode");
            }

            string SQL = "select bl.Barcode from INVENTORY_ACTIVITY ia join ITEM_MOVEMENT im on ia.ActivityId = im.ActivityId left join BARCODE_LOG bl on im.ItemBarcodeId = bl.BarcodeID left join PRINT_INSTANCE pinst on pinst.InstanceId = bl.PrintInstanceId LEFT JOIN STORAGE_LOCATION SL ON IM.LocationId = SL.LocationId where ia.RefDocIdentifier = '" + JsonConvert.SerializeObject(DocumentIdentifier) + "' and ia.Direction =" + (int)movDir +" AND pinst.ItemCode =" + itemCode;
            List<dynamic> itemsStatus = DBManager.SelectObjects<dynamic>(SQL);
            return itemsStatus;
        }


        /*Returns if a list of barcodes are valid*/
        public static bool AreItemBarcodesValid( List <string> barcodesToCheck )
        {
            var distinctBarcodes = new HashSet<string>(barcodesToCheck);
            
            if (distinctBarcodes.Count == 0) {
                throw new Exception("No item barcodes to check");
            }

            string SQL = $@"select count(*) as Count from BARCODE_LOG bl 
                            where bl.Barcode in ( '"+ String.Join("','", distinctBarcodes) +"') ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;

            return count == distinctBarcodes.Count;
        }


        /*Returns if a list of location barcodes are valid*/
        public static bool AreLocationBarcodesValid(List<string> barcodesToCheck)
        {
            var distinctBarcodes = new HashSet<string>(barcodesToCheck);

            if (distinctBarcodes.Count == 0)
            {
                throw new Exception("No location barcodes to check");
            }

            string SQL = $@"select count(*) as Count from STORAGE_LOCATION sl 
                            where sl.Barcode in ( '" + String.Join("','", distinctBarcodes) + "') ;";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;

            return count == distinctBarcodes.Count;
        }


        /*
         * Checks if all the supplied barcodes are located only in the supplied ERP Location.
         */
        public static bool ValidateLocationBarcodes(List<string> locationBarcodes, ERPLocation location) {
            var distinctBarcodes = new HashSet<string>(locationBarcodes);
            
            string SQL = $@"select count(*) as Count 
                            from STORAGE_LOCATION 
                            where barcode in ('" + String.Join("','", distinctBarcodes) + $@"')
                            and Aisle = "+ location.Id + ";";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;

            return distinctBarcodes.Count == count;
        }


        /*
         * Checks if all the supplied barcodes are located only in the supplied ERP Location.
         */
        public static bool ValidateItemsAreAtLocation(List<string> itemBarcodes, ERPLocation location)
        {
            var distinctBarcodes = new HashSet<string>(itemBarcodes);

            string SQL = $@"select count(*) as Count
                            from ITEM_MOVEMENT im
                            join BARCODE_LOG bl 
                            on im.ItemBarcodeId = bl.BarcodeID
                            join STORAGE_LOCATION sl
                            on im.LocationId = sl.LocationId
                            where
                            im.IsActive = 1
                            and 
                            bl.Barcode in ('" + String.Join("','", distinctBarcodes) + $@"')
                            and sl.Aisle in (" +location.Id+")";

            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;

            return distinctBarcodes.Count == count;
        }


        /*
         * Checks if all the supplied item barcodes are instances of only the supplied items.
         */
        public static bool ValidateItemBarcodes(List<string> itemBarcodes, List<DocumentItem> documentItems)
        {
            var distinctBarcodes = new HashSet<string>(itemBarcodes);
            var itemCodes = documentItems.ConvertAll<string>(dItm => dItm.Code);
            
            string SQL = $@"select count(*) as Count from BARCODE_LOG bl 
                            join PRINT_INSTANCE pi 
                            on bl.PrintInstanceId = pi.InstanceId
                            where bl.Barcode in ( '" + String.Join("','", distinctBarcodes) + $@"')
                            and pi.ItemCode  in ( '" + String.Join("','", itemCodes ) +"') ; ";
            var count = DBManager.SelectObjects<dynamic>(SQL).SingleOrDefault().Count;

            return distinctBarcodes.Count == count;
        }
    }
}
