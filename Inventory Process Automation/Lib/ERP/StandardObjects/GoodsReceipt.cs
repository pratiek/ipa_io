﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using IPA.Lib.Util;
using Newtonsoft.Json;


namespace IPA.Lib.ERP.StandardObjects
{
    public class GoodsReceipt
    {
        public static List<DocumentItem> GetDocumentItems(dynamic Filters) {
            Type targetClass = ERPRouter.GetObject(ObjectNames.GOODS_RECEIPT);

            dynamic docIdentifier = new ExpandoObject();
            docIdentifier.DocumentType = DocumentTypes.GOODS_RECEIPT;
            docIdentifier.VoucherNo = Filters.VoucherNo;
            docIdentifier.FormCode = Filters.FormCode;

            dynamic items =  targetClass.GetMethod("GetDocumentItems").Invoke(null, new object[] { Filters });

            foreach (DocumentItem dItm in items)
            {
                dItm.PrintCount = MovementUtil.GetMovementCount(docIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                List<dynamic> barCodes = MovementUtil.GetMovedItems(docIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                dItm.ReleasedItems = barCodes;
                /*List<dynamic> itemsStatus = DBManager.SelectObjects<dynamic>("select sum(PrintQuantity) as totalPrinted from PRINT_INSTANCE WHERE ItemCode =" + dItm.Code + " AND ef;");
                var resRow = itemsStatus.First();
                dItm.PrintCount = resRow.totalPrinted == null ? 0 : resRow.totalPrinted;*/

            }

            return items; 
        }

        public static List<Document> GetDocuments(dynamic Filters)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.GOODS_RECEIPT);
            dynamic method = targetClass.GetMethod("GetDocuments");
            dynamic items =  method.Invoke(null, new Object[] { Filters });
            return items; 
        }
    }
}
