﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.Models;
using IPA.Lib.Util;
using Newtonsoft.Json;


namespace IPA.Lib.ERP.StandardObjects
{
    public class PurchaseReturn
    {
        public static List<DocumentItem> GetDocumentItems(dynamic DocumentIdentifier)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.PURCHASE_RETURN);
            dynamic items = targetClass.GetMethod("GetDocumentItems").Invoke(null, new object[] { DocumentIdentifier });
            foreach (DocumentItem dItm in items)
            {
                var itemCode = dItm.Code;
                var count = MovementUtil.GetMovementCount(DocumentIdentifier, MOVEMENT_DIRECTION.OUTWARDS, itemCode);
                List<dynamic> barCodes = MovementUtil.GetMovedItems(DocumentIdentifier, MOVEMENT_DIRECTION.OUTWARDS, itemCode);
                dItm.PrintCount = count;
                dItm.ReleasedItems = barCodes;
            }
            return items;
        }

        public static List<Document> GetDocuments(dynamic Filters)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.PURCHASE_RETURN);
            dynamic method = targetClass.GetMethod("GetDocuments");
            dynamic items = method.Invoke(null, new Object[] { Filters });
            return items;
        }
    }
}
