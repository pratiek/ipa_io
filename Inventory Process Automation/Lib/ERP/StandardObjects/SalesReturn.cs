﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using IPA.Lib.Util;
using Newtonsoft.Json;

namespace IPA.Lib.ERP.StandardObjects
{
    public class SalesReturn
    {
        public static List<DocumentItem> GetDocumentItems(dynamic DocumentIdentifier)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.SALES_RETURN);

            dynamic docIdentifier = new ExpandoObject();
            docIdentifier.DocumentType = DocumentTypes.SALES_RETURN;
            docIdentifier.VoucherNo = DocumentIdentifier.VoucherNo;
            docIdentifier.FormCode = DocumentIdentifier.FormCode;

            dynamic items = targetClass.GetMethod("GetDocumentItems").Invoke(null, new object[] { DocumentIdentifier });
            foreach (DocumentItem dItm in items)
            {
                dItm.PrintCount = MovementUtil.GetMovementCount(docIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                List<dynamic> barCodes = MovementUtil.GetMovedItems(docIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                dItm.ReleasedItems = barCodes;
                /*List<dynamic> itemsStatus = DBManager.SelectObjects<dynamic>("select sum(PrintQuantity) as totalPrinted from PRINT_INSTANCE WHERE ItemCode =" + dItm.Code + ";");
                var resRow = itemsStatus.First();
                dItm.PrintCount = resRow.totalPrinted == null ? 0 : resRow.totalPrinted;
                List<dynamic> barCodes = MovementUtil.GetMovedItems(DocumentIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                dItm.ReleasedItems = barCodes;
                dItm.PrintCount = MovementUtil.GetMovementCount(DocumentIdentifier, MOVEMENT_DIRECTION.INWARDS, dItm.Code);
                */
            }
            return items;
        }

        public static List<Document> GetDocuments(dynamic Filters)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.SALES_RETURN);
            dynamic method = targetClass.GetMethod("GetDocuments");
            dynamic items = method.Invoke(null, new Object[] { Filters });
            return items;
        }
    }
}
