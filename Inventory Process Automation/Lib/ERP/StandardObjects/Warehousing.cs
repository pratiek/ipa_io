﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using Newtonsoft.Json;

namespace IPA.Lib.ERP.StandardObjects
{
    public class Warehousing
    {
        public static List<ERPLocation> GetLocations(dynamic Filters)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.WAREHOUSING);
            dynamic method = targetClass.GetMethod("GetLocations");
            dynamic items = method.Invoke(null, new Object[] { Filters });
            return items;
        }
    }
}
