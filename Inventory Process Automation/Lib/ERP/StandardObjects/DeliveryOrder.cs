﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Models;
using Newtonsoft.Json;
using IPA.Lib.Util;

namespace IPA.Lib.ERP.StandardObjects
{
    public class DeliveryOrder
    {
        public static List<DocumentItem> GetDocumentItems(dynamic DocumentIdentifier)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.DISPATCH_ORDER);
            dynamic items = targetClass.GetMethod("GetDocumentItems").Invoke(null, new object[] { DocumentIdentifier });

            foreach (DocumentItem dItm in items)
            {
                var itemCode = dItm.Code;
                var count = MovementUtil.GetMovementCount(DocumentIdentifier, MOVEMENT_DIRECTION.OUTWARDS, itemCode);
                List<dynamic> barCodes = MovementUtil.GetMovedItems(DocumentIdentifier, MOVEMENT_DIRECTION.OUTWARDS, itemCode);
                dItm.PrintCount = count;
                dItm.ReleasedItems = barCodes;
            }
            return items;
        }

        public static List<Document> GetDocuments(dynamic Filters)
        {
            Type targetClass = ERPRouter.GetObject(ObjectNames.DISPATCH_ORDER);
            dynamic method = targetClass.GetMethod("GetDocuments");
            dynamic items = method.Invoke(null, new Object[] { Filters });
            
            return items;
        }
    }
}
