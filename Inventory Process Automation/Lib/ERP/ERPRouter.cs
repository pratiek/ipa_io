﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.ERP.ERPSpecifics;

namespace IPA.Lib.ERP
{
    public class ERPRouter
    {
        //TODO: inject logic to identify which ERP to connect to.
        public static Type GetObject(ObjectNames DocumentType) {
           
            switch (DocumentType)
            {
                case ObjectNames.DISPATCH_ORDER:
                    return typeof(ERPSpecifics.SynergyERP.DeliveryOrder);

                case ObjectNames.GOODS_RECEIPT:
                    return typeof(ERPSpecifics.SynergyERP.GoodsReceipt);

                case ObjectNames.PURCHASE_RETURN:
                    return typeof(ERPSpecifics.SynergyERP.PurchaseReturn);

                case ObjectNames.SALES_RETURN:
                    return typeof(ERPSpecifics.SynergyERP.SalesReturn);

                case ObjectNames.INVENTORY_TRANSFER_OUT:
                    return typeof(ERPSpecifics.SynergyERP.InventoryTransferOut);

                case ObjectNames.INVENTORY_TRANSFER_IN:
                    return typeof(ERPSpecifics.SynergyERP.InventoryTransferIn);

                case ObjectNames.WAREHOUSING:
                    return typeof(ERPSpecifics.SynergyERP.Warehousing);
            }

            return null;
        }

    }
}
