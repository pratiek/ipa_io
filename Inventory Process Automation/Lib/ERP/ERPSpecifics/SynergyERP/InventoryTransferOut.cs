﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.ERP.StandardObjects;
using IPA.Lib.Core.Database;
using System.Data;
using IPA.Lib.Models;
using System.Dynamic;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class InventoryTransferOut
    {
        public static List<DocumentItem> GetDocumentItems(dynamic Filters)
        {
            string DocumentId = Filters.VoucherNo;
            string FormCode = Filters.FormCode;

            string sql = $@"SELECT A.ITEM_CODE, B.ITEM_EDESC,
                                A.QUANTITY,
                                A.SERIAL_NO,
                                A.MU_CODE,
                                A.FORM_CODE 
                                FROM IP_TRANSFER_ISSUE  A,
                                IP_ITEM_MASTER_SETUP B
                               WHERE A.ITEM_CODE = B.ITEM_CODE
                               AND A.COMPANY_CODE = B.COMPANY_CODE
                               AND A.ISSUE_NO = '{DocumentId}'
                               AND A.FORM_CODE = '{FormCode}'
                               AND A.COMPANY_CODE = '{Filters.CompanyCode}'
                               AND A.BRANCH_CODE = '{Filters.BranchCode}'";

            DataTable results = Query.RunQuery(sql);

            List<DocumentItem> docItems = new List<DocumentItem>();
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    DocumentItem dItm = new DocumentItem
                    {
                        Code = dr["ITEM_CODE"].ToString(),
                        ItemDescription = dr["ITEM_EDESC"].ToString(),
                        Quantity = dr["QUANTITY"].ToString()
                    };
                    docItems.Add(dItm);
                }
            }
            return docItems;
        }


        public static List<Document> GetDocuments(dynamic Filters)
        {
            string sql = $@"SELECT 
                        DISTINCT  
                        A.ISSUE_NO VOUCHER_NO, 
                        A.FORM_CODE,
                        A.ISSUE_DATE,
                        D.LOCATION_EDESC LOCATION_NAME

                        FROM IP_TRANSFER_ISSUE  A
                        JOIN IP_LOCATION_SETUP D
                            ON A.TO_LOCATION_CODE = D.LOCATION_CODE
                        WHERE  FORM_CODE IN (SELECT FORM_CODE FROM FORM_DETAIL_SETUP 
                        WHERE TABLE_NAME = 'IP_TRANSFER_ISSUE' AND
                        COMPANY_CODE = A.COMPANY_CODE)
                        AND A.COMPANY_CODE = '{Filters.CompanyCode}' 
                        AND A.BRANCH_CODE = '{Filters.BranchCode}'";

            List<Document> documents = new List<Document>();

            DataTable results = Query.RunQuery(sql);
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    dynamic docIdentifier = new ExpandoObject();
                    docIdentifier.DocumentType = DocumentTypes.INVENTORY_TRANSFER_OUT;
                    docIdentifier.VoucherNo = dr["VOUCHER_NO"].ToString();
                    docIdentifier.FormCode = dr["FORM_CODE"].ToString();

                    dynamic docData = new ExpandoObject();
                    docData.LocationName = dr["LOCATION_NAME"].ToString();

                    Document doc = new Document
                    {
                        DocumentDate = dr["ISSUE_DATE"].ToString(),
                        DocumentIdentifier = docIdentifier,
                        DocumentData = docData
                    };

                    documents.Add(doc);
                }
            }
            return documents;
        }
    }
}
