﻿using IPA.Lib.Core.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Models;
using IPA.Lib.ERP.StandardObjects;
using System.Dynamic;
using IPA.Lib.Models.ParameterModels;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class CompanyList
    {
        public static List<CompanyListing> GetCompanyList ()
        {
            string sql = $@"SELECT COMPANY_CODE, company_edesc FROM company_setup";
            DataTable results = Query.RunQuery(sql);

            List<CompanyListing> compList = new List<CompanyListing>();

            if(results != null)
            {
                foreach(DataRow dr in results.Rows)
                {
                    CompanyListing cList = new CompanyListing
                    {
                        CompanyCode = dr["COMPANY_CODE"].ToString(),
                        CompanyName = dr["COMPANY_EDESC"].ToString()
                    };
                    compList.Add(cList);
                }
            }


            return compList;
        }
        }
    }
