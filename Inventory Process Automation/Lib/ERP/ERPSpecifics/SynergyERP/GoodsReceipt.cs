﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.ERP.StandardObjects;
using IPA.Lib.Core.Database;
using System.Data;
using IPA.Lib.Models;
using System.Dynamic;
using IPA.Lib.Core.IPADatabase;
using IPA.Lib.Core.Authentication;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class GoodsReceipt : ERPDocument
    {
        /* DO NOT CHANGE the function name, ever. 
        Even the IDEs rename tool will definitely break things.
        Unless of course, you know what you are doing. */
        public static List<DocumentItem> GetDocumentItems(dynamic Filters){//String DocumentId, string FormCode) {
            string VoucherNo = Filters.VoucherNo;
            string FormCode = Filters.FormCode;

            string sql = $@"SELECT  
                            A.MRR_NO,
                            A.ITEM_CODE, 
                            B.ITEM_EDESC,  
                            A.QUANTITY,
                            A.SERIAL_NO,
                            A.MU_CODE,
                            A.FORM_CODE 
                            FROM IP_PURCHASE_MRR  A, IP_ITEM_MASTER_SETUP B 
                            WHERE 
                            A.ITEM_CODE = B.ITEM_CODE 
                            AND A.COMPANY_CODE = B.COMPANY_CODE 
                            AND A.MRR_NO = '{VoucherNo}' 
                            and A.FORM_CODE = '{FormCode}' 
                            AND A.COMPANY_CODE = '{Filters.CompanyCode}'
                            AND A.BRANCH_CODE = '{Filters.BranchCode}'";

            DataTable results =  Query.RunQuery(sql);

            List<DocumentItem> docItems = new List<DocumentItem>();
            if (results != null) {
                foreach (DataRow dr in results.Rows)
                {
                    
                    var itemCode = dr["ITEM_CODE"].ToString();
                    var quantity = dr["QUANTITY"].ToString();


                    //var itemStatus = (status + "/" + quantity);
                    DocumentItem dItm = new DocumentItem {
                        Code = itemCode,
                        ItemDescription = dr["ITEM_EDESC"].ToString(),
                        Quantity = quantity,
                        //Status = itemStatus,
                       // StatusCount = status
                    };
                    docItems.Add(dItm);
                }
            }
            return docItems;
        }


        public static List<Document> GetDocuments(dynamic Filters) {

            string sql = $@"SELECT 
                            DISTINCT  MRR_NO VOUCHER_NO,
                            FORM_CODE ,
                            MRR_DATE,
                            A.SUPPLIER_CODE, 
                            A.TO_LOCATION_CODE,
                            D.LOCATION_EDESC LOCATION_NAME,
                            S.SUPPLIER_EDESC SUPPLIER_NAME
                            FROM IP_PURCHASE_MRR  A
                            JOIN IP_SUPPLIER_SETUP S
                            ON A.SUPPLIER_CODE = S.SUPPLIER_CODE
                            JOIN IP_LOCATION_SETUP D
                            ON A.TO_LOCATION_CODE = D.LOCATION_CODE
                            WHERE  
                            FORM_CODE IN 
	                            (
		                            SELECT 
		                            FORM_CODE FROM
		                            FORM_DETAIL_SETUP 
		                            WHERE 
		                            TABLE_NAME = 'IP_PURCHASE_MRR' 
		                            AND COMPANY_CODE = A.COMPANY_CODE
	                            ) 
                            AND MRR_DATE BETWEEN  TO_DATE('01/01/1970','mm/dd/yyyy') 
                            AND TO_DATE('{ DateTime.Today.ToString("MM/dd/yyyy")}','mm/dd/yyyy')
                            AND A.COMPANY_CODE = '{Filters.CompanyCode}'
                            AND A.BRANCH_CODE = '{Filters.BranchCode}'
                            AND UPPER(MRR_NO) like UPPER('%%')
                            ORDER BY MRR_DATE DESC";

            List<Document> documents = new List<Document>();

            DataTable results =  Query.RunQuery(sql);
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    dynamic docIdentifier = new ExpandoObject();
                    docIdentifier.DocumentType = DocumentTypes.GOODS_RECEIPT;
                    docIdentifier.VoucherNo = dr["VOUCHER_NO"].ToString();
                    docIdentifier.FormCode = dr["FORM_CODE"].ToString();

                    dynamic docData = new ExpandoObject();
                    docData.SupplierName = dr["SUPPLIER_NAME"].ToString();
                    docData.LocationName = dr["LOCATION_NAME"].ToString();
                    docData.CompanyCode = "HEPL";
                    docData.Status = "On Progress";

                    Document doc = new Document
                    {
                        DocumentDate = dr["MRR_DATE"].ToString(),
                        DocumentIdentifier = docIdentifier,
                        DocumentData = docData
                    };

                    documents.Add(doc);
                }
            }
            return documents;
        }
    }
}
