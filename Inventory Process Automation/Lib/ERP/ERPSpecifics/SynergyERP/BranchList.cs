﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Models;
using IPA.Lib.Core.Database;
using IPA.Lib.Models.ParameterModels;
using IPA.Lib.ERP.StandardObjects;
using System.Dynamic;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class BranchList
    {
        public static List<BranchListing> GetBranchList(String CompanyCode)
        {
            string sql = $@"select branch_code, branch_edesc, company_code from fa_branch_setup
                        where group_sku_flag='I' AND deleted_flag='N' AND company_code ={CompanyCode}
                        ORDER BY branch_code";
             DataTable results = Query.RunQuery(sql);

            List<BranchListing> BranList = new List<BranchListing>();

            if (results != null)
            {
                foreach(DataRow dr in results.Rows)
                {
                    BranchListing bList = new BranchListing
                    {
                        BranchName = dr["BRANCH_EDESC"].ToString(),
                        BranchCode = dr["BRANCH_CODE"].ToString()
                    };
                    BranList.Add(bList);
                }
            }
            return BranList;
        }
    }
}
