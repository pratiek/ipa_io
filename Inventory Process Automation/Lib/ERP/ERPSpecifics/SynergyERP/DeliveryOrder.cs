﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IPA.Lib.ERP.StandardObjects;
using IPA.Lib.Core.Database;
using System.Data;
using IPA.Lib.Models;
using System.Dynamic;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class DeliveryOrder
    {
        public static List<DocumentItem> GetDocumentItems(dynamic Filters)
        {//String DocumentId, string FormCode) {
            //string DocumentId = Filters.VoucherNo;
            //string FormCode = Filters.FormCode;

            string sql = $@"SELECT A.ITEM_CODE
                        	,B.ITEM_EDESC
                        	,A.QUANTITY
                        	,A.SERIAL_NO
                        	,A.MU_CODE
                        	,A.FORM_CODE
                        FROM SA_SALES_CHALAN A
                        	,IP_ITEM_MASTER_SETUP B
                        WHERE A.ITEM_CODE = B.ITEM_CODE
                        	AND A.COMPANY_CODE = B.COMPANY_CODE
                        	AND A.CHALAN_NO = '{Filters.VoucherNo}'
                        	AND A.FORM_CODE = '{Filters.FormCode}'
                        	AND A.COMPANY_CODE = '{Filters.CompanyCode}'
                            AND A.BRANCH_CODE = '{Filters.BranchCode}'";

            DataTable results = Query.RunQuery(sql);

            List<DocumentItem> docItems = new List<DocumentItem>();
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    DocumentItem dItm = new DocumentItem
                    {
                        Code = dr["ITEM_CODE"].ToString(),
                        ItemDescription = dr["ITEM_EDESC"].ToString(),
                        Quantity = dr["QUANTITY"].ToString()
                    };
                    docItems.Add(dItm);
                }
            }
            return docItems;
        }


        public static List<Document> GetDocuments(dynamic Filters)
        {

            string sql = $@"SELECT DISTINCT CHALAN_NO VOUCHER_NO
                            	,FORM_CODE
                                ,CHALAN_DATE
                                ,A.CUSTOMER_CODE
                                ,S.CUSTOMER_EDESC CUSTOMER_NAME  
                            FROM SA_SALES_CHALAN A
                            JOIN SA_CUSTOMER_SETUP S
                            ON A.CUSTOMER_CODE = S.CUSTOMER_CODE
                            WHERE FORM_CODE IN (
                            		SELECT FORM_CODE
                            		FROM FORM_DETAIL_SETUP
                            		WHERE TABLE_NAME = 'SA_SALES_CHALAN'
                            			AND COMPANY_CODE = A.COMPANY_CODE
                            		)
                            	AND CHALAN_DATE BETWEEN TO_DATE('01/01/1970','mm/dd/yyyy') 
                            		AND TO_DATE('{DateTime.Today.ToString("MM/dd/yyyy")}','mm/dd/yyyy')
                            	AND A.COMPANY_CODE = '{Filters.CompanyCode}'
                            AND A.BRANCH_CODE = '{Filters.BranchCode}'
                            AND UPPER(CHALAN_NO) LIKE UPPER('%%')
                                ORDER BY CHALAN_DATE DESC";

            List<Document> documents = new List<Document>();

            DataTable results = Query.RunQuery(sql);
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    dynamic docIdentifier = new ExpandoObject();
                    docIdentifier.DocumentType = DocumentTypes.DELIVERY_ORDER;
                    docIdentifier.VoucherNo = dr["VOUCHER_NO"].ToString();
                    docIdentifier.FormCode = dr["FORM_CODE"].ToString();

                    dynamic docData = new ExpandoObject();
                    docData.CustomerName = dr["CUSTOMER_NAME"].ToString();

                    Document doc = new Document
                    {
                        DocumentDate = dr["CHALAN_DATE"].ToString(),
                        DocumentIdentifier = docIdentifier,
                        DocumentData = docData
                    };

                    documents.Add(doc);
                }
            }
            return documents;
        }
    }
}
