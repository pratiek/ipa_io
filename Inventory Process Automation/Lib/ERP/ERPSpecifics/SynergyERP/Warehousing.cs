﻿using IPA.Lib.Core.Database;
using IPA.Lib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class Warehousing
    {
        public static List<ERPLocation> GetLocations(dynamic Filters)
        {
            string sql = $@"select LOCATION_CODE, 
                            LOCATION_EDESC as LOCATION_NAME,  
                            COMPANY_CODE 
                            from IP_LOCATION_SETUP 
                            where GROUP_SKU_FLAG = 'I' 
                            and DELETED_FLAG= 'N' 
                            and company_code='{Filters.CompanyCode}'";

            List<ERPLocation> locations = new List<ERPLocation>();

            DataTable results = Query.RunQuery(sql);
            if (results != null)
            {
                foreach (DataRow dr in results.Rows)
                {
                    dynamic Identifier = new ExpandoObject();
                    //Identifier.DocumentType = ;
                    Identifier.LocationCode = dr["LOCATION_CODE"].ToString();
                    Identifier.CompanyCode = dr["COMPANY_CODE"].ToString();

                    ERPLocation loc = new ERPLocation
                    {
                        Identifier = Identifier,
                        Name = dr["LOCATION_NAME"].ToString()
                    };

                    locations.Add(loc);
                }
            }
            return locations;
        }
    }
}
