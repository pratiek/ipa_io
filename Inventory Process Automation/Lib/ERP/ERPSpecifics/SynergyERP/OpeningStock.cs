﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IPA.Lib.Models;
using IPA.Lib.Core.Database;
using IPA.Lib.Models.ParameterModels;
using IPA.Lib.ERP.StandardObjects;
using System.Dynamic;
using IPA.Lib.Util;
using Microsoft.AspNetCore.Authorization;
using IPA.Lib.Core.Authentication;

namespace IPA.Lib.ERP.ERPSpecifics.SynergyERP
{
    public class OpeningStock
    {
        
        public static List<dynamic> GetOpeningStock(string docIdentifier)
        {
            string sql = $@"select im.item_code, 
                            im.item_edesc, 
                            os.company_code, 
                            os.branch_code, 
                            os.stock_balance, 
                            im.created_date 
                            from IP_ITEM_MASTER_SETUP im 
                            join 
                            (select item_code,company_code, branch_code, sum(in_quantity) - sum(out_quantity) as stock_balance 
                            from v$virtual_stock_wip_ledger1 
                            group by 
                            item_code, 
                            company_code,
                            branch_code )  
                            os on 
                            im.item_code = os.item_code 
                            where 
                            im.GROUP_SKU_FLAG = 'I'
                            and os.company_code = '"+AuthManager.GetCurrentUser().CompanyCode + $@"'
                            and os.branch_code = '" + AuthManager.GetCurrentUser().BranchCode+ $@"'
                            and os.stock_balance > 0
                        ";
            DataTable results = Query.RunQuery(sql);

            List<dynamic> BranList = new List<dynamic>();

            if (results != null)
            {
                foreach(DataRow dr in results.Rows)
                {
                    dynamic openingStock = new ExpandoObject();
                    openingStock.itemCode = dr["ITEM_CODE"].ToString();
                    openingStock.itemDesc = dr["ITEM_EDESC"].ToString();
                    openingStock.companyCode = dr["COMPANY_CODE"].ToString();
                    openingStock.date = dr["CREATED_DATE"].ToString();
                    openingStock.quantity = dr["STOCK_BALANCE"].ToString();
                    //openingStock.status = MovementUtil.GetMovementCount(docIdentifier, MOVEMENT_DIRECTION.INWARDS, openingStock.itemCode);

                    BranList.Add(openingStock);
                }
            }
            return BranList;
        }
    }
}
