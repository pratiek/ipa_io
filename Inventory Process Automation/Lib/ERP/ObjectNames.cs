﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IPA.Lib.ERP
{
    public enum ObjectNames
    {
        GOODS_RECEIPT = 1,
        DISPATCH_ORDER,
        PURCHASE_RETURN,
        SALES_RETURN,
        INVENTORY_TRANSFER_OUT,
        INVENTORY_TRANSFER_IN,
        WAREHOUSING,
        INTRA_WAREHOUSE_TRANSFER,
        OPENING_STOCK
    }
}
